from odoo import models, fields, tools


class loyal_customers_report(models.Model):
    _name = "crm.sale.loyal.customers.report"
    _description = "Loyal Customers Report"
    _auto = False

    construction_step = fields.Many2one('crm.payphase')
    customer = fields.Many2one('res.partner')
    property_owner = fields.Many2one('crm.property.owners')
    property_site = fields.Many2one('crm.sites', )
    property = fields.Many2one('crm.site.properties')
    done_date = fields.Date('Done Date')
    payment_date = fields.Date('Payment Date')
    days = fields.Integer('Days Difference')
    customer_status = fields.Char('Customer Status')
    payment_status = fields.Char('Payment Status')

    def _send_warninig_on_late_payments(self):
        loyal_customer_reports = self.env["crm.sale.loyal.customers.report"].search(
            [('customer_status', 'in', ['Warning', 'Termination Stage']), ('payment_status', '=', 'Not Paid')])

        channels = self.env['mail.channel'].search([('name', '=', 'sales')])

        odoobot = self.env.ref('base.partner_root')

        for channel in channels:
            for m in loyal_customer_reports:
                message = str(m.customer.name) + " didn’t pay due for the " + str(
                    m.construction_step.name) + " stage that is done on " + str(m.done_date) + ",the client is on the " \
                    + str(m.customer_status) + " stage since " + \
                    str(int(m.days)) + " days are passed"
                channel.message_post(
                    subject="Unpaid Dues",
                    body=message,
                    message_type='comment',
                    subtype_xmlid='mail.mt_comment',
                    author_id=odoobot.id,
                )

    def init(self):
        tools.drop_view_if_exists(self.env.cr, self._table)
        self.env.cr.execute("""CREATE or REPLACE VIEW %s as (%s)""" % (self._table,
                                                                       """ select 
x.id,x.construction_step,x.customer,x.property_owner,x.property_site,x.property,x.done_date,x.payment_date,x.days,
case when x.days<=15 then 'Loyal Customer'
     when x.days<=45 then 'Warning'
	 else 'Termination Stage' end as customer_status,case when x.payment_date IS NULL then 'Not Paid' else 'Paid' end as payment_status

from(
select ROW_NUMBER () OVER (
           ORDER BY a.customer
        ) as id,b.name as construction_step,a.customer,a.property_owner,a.property_site,a.property ,
       b.done_date,c.payment_date,
	case when c.payment_date IS NULL then DATE_PART('day',  CURRENT_TIMESTAMP - b.done_date::timestamp)  
ELSE DATE_PART('day',  c.payment_date - b.done_date::timestamp) END AS days
	
from crm_sales a 
inner join crm_construction_progress b on a.id=b.sales_id
full join crm_construction_progress_payment c on b.id=c.construction_progress_id)x where x.done_date is not null
  """))
