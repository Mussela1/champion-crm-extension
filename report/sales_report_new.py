from odoo import api, fields, models
from io import BytesIO
import xlsxwriter
import datetime
from odoo.exceptions import UserError

try:
    from base64 import encodebytes
except ImportError:
    from base64 import encodestring as encodebytes


class SalesFullReport(models.TransientModel):
    _name = 'crm.sales.report'

    def generate_sales_xlsx_report(self, workbook):
        sheet = workbook.add_worksheet('Sales Report')

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 30)
        sheet.set_column('F:F', 30)
        sheet.set_column('G:G', 30)
        sheet.set_column('H:H', 30)
        sheet.set_column('I:I', 30)
        sheet.set_column('J:J', 30)
        sheet.set_column('K:K', 30)
        sheet.set_column('L:L', 30)
        sheet.set_column('M:M', 30)
        sheet.set_column('N:N', 30)
        sheet.set_column('O:O', 30)
        sheet.set_column('P:P', 30)
        sheet.set_column('Q:Q', 30)

        # get sales report
        sales = self.env['crm.sales'].search([])
