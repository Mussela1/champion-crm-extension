from odoo import api, fields, models
from io import BytesIO
import xlsxwriter
import datetime
from odoo.exceptions import UserError
import pandas as pd

try:
    from base64 import encodebytes
except ImportError:
    from base64 import encodestring as encodebytes



class sales_report_wizard(models.TransientModel):
    _name = 'crm.sales.report.wizard'

    user_id = fields.Many2one(
        'res.users', string='Salesperson', default=lambda self: self.env.user)
    lead_source = fields.Many2one('crm.lead.source', 'Lead Sources')
    lead_source_category = fields.Many2one('crm.lead.source.category', 'Source Category',
                                           domain="[('source_id', '=', lead_source)]")
    date_from = fields.Date('Date From')
    date_to = fields.Date('Date To')
    developer = fields.Many2one('crm.property.owners', string='Developer')
    site = fields.Many2one('crm.sites',
                           string='Site', domain="[('owner_id', '=', developer)]")
    fileout = fields.Binary('File', readonly=True)

    def col_num_to_letter(self, col_num):
        """Convert a column number (e.g., 1) to its letter (e.g., 'A')."""
        letter = ''
        while col_num > 0:
            col_num, remainder = divmod(col_num - 1, 26)
            letter = chr(65 + remainder) + letter
        return letter

    def print_crm_sales_report(self):
        domains = []
        if self.user_id:
            domains.append(('user_id', '=', self.user_id.id))

        # tests = self.env['crm.lead'].search(domains)
        tests = []

        # get likes,followers
        self.env.cr.execute("""  select sum(b.posts) as posts,sum(b.followers) as followers,sum(b.likes) as likes,sum(b.post_reach) as post_reach,sum(b.no_comments) as no_comments,sum(b.no_shares) as no_shares
                                 from crm_social_media_record a inner join crm_social_media_record_detail b on a.id=b.detail_id
                                 where a.date_from between %s and %s and a.sales_personnel=%s
                                 group by a.sales_personnel """, (self.date_from, self.date_to, self.user_id.id,))
        results = self.env.cr.dictfetchone()
        # results = None

        # Leads
        if self.site:
            self.env.cr.execute(
                """ select count(*) as total_leads from crm_lead where user_id=%s and date_open between %s and %s and is_started_as_lead=true and property_site=%s """,
                (self.user_id.id, self.date_from, self.date_to, self.site.id))
            leads = self.env.cr.dictfetchone()

            self.env.cr.execute(""" select count(*) as total_opportunity from crm_lead where user_id=%s and date_open between %s and %s and type='opportunity' and property_site=%s
                """, (self.user_id.id, self.date_from, self.date_to, self.site.id))
            opportunity = self.env.cr.dictfetchone()

            # won
            self.env.cr.execute("""select count(*) as total_won from crm_lead cl inner join crm_stage cs on cl.stage_id=cs.id
                                    where cl.user_id=%s and cl.date_open between %s and %s and type='opportunity' and cs.is_won=true and cl.property_site=%s""",
                                (self.user_id.id, self.date_from, self.date_to, self.site.id))
            closed_deals = self.env.cr.dictfetchone()

            self.env.cr.execute(""" select mat.name as activity_type,count(*) as cnt from crm_lead c inner join mail_activity ma on c.id=ma.res_id inner join mail_activity_type mat on ma.activity_type_id=mat.id
                                    where c.user_id=%s and ma.date_done between %s and %s and c.property_site=%s
                                    group by mat.name""", (self.user_id.id, self.date_from, self.date_to, self.site.id))
            activity_types = self.env.cr.dictfetchall()

        else:
            self.env.cr.execute(
                """ select count(*) as total_leads from crm_lead where user_id=%s and date_open between %s and %s and is_started_as_lead=true """,
                (self.user_id.id, self.date_from, self.date_to))
            leads = self.env.cr.dictfetchone()

            self.env.cr.execute(""" select count(*) as total_opportunity from crm_lead where user_id=%s and date_open between %s and %s and type='opportunity'
                """, (self.user_id.id, self.date_from, self.date_to))
            opportunity = self.env.cr.dictfetchone()

            # won
            self.env.cr.execute("""select count(*) as total_won from crm_lead cl inner join crm_stage cs on cl.stage_id=cs.id
                                    where cl.user_id=%s and cl.date_open between %s and %s and type='opportunity' and cs.is_won=true""",
                                (self.user_id.id, self.date_from, self.date_to))
            closed_deals = self.env.cr.dictfetchone()

            # activities
            self.env.cr.execute(""" select mat.name as activity_type,count(*) as cnt from crm_lead c inner join mail_activity ma on c.id=ma.res_id inner join mail_activity_type mat on ma.activity_type_id=mat.id
                                    where c.user_id=%s and ma.date_done between %s and %s 
                                    group by mat.name""", (self.user_id.id, self.date_from, self.date_to))
            activity_types = self.env.cr.dictfetchall()

        office_visit = 0
        site_visit = 0
        site_office_visit = 0
        inbound_call = 0
        outbound_call = 0

        for activity_type in activity_types:
            if activity_type["activity_type"] == "Office Visit":
                office_visit = activity_type["cnt"]
            elif activity_type["activity_type"] == "Site Visit":
                site_visit = activity_type["cnt"]
            elif activity_type["activity_type"] == "Office and Site Visit":
                site_office_visit = activity_type["cnt"]
            elif activity_type["activity_type"] == "Inbound Call":
                inbound_call = activity_type["cnt"]
            elif activity_type["activity_type"] == "Outbound Call":
                outbound_call = activity_type["cnt"]

        docargs = {
            'doc_ids': self.ids,
            'developer': self.developer.name,
            'site': self.site.name,
            'sales_person': self.user_id.name,
            'date_from': self.date_from.strftime('%d-%b-%Y'),
            'date_to': self.date_to.strftime('%d-%b-%Y'),
            'posts': results['posts'] if results is not None else 0,
            'followers': results['followers'] if results is not None else 0,
            'likes': results['likes'] if results is not None else 0,
            'post_reach': results['post_reach'] if results is not None else 0,
            'no_comments': results['no_comments'] if results is not None else 0,
            'no_shares': results['no_shares'] if results is not None else 0,
            'leads': leads['total_leads'] if leads is not None else 0,
            'opportunity': opportunity['total_opportunity'] if opportunity is not None else 0,
            'office_visit': office_visit,
            'site_visit': site_visit,
            'site_office_visit': site_office_visit,
            'inbound_call': inbound_call,
            'outbound_call': outbound_call,
            'closed_deals': closed_deals['total_won'] if closed_deals is not None else 0,

        }

        return self.env.ref('champion_crm.crm_action_sales_report').report_action(tests, docargs)

    # raise UserError(_('No result found!'))
    def print_crm_performance_report(self):
        domains = []
        tests = []

        # Leads
        self.env.cr.execute(
            """ select count(*) as total_leads from crm_lead where  date_open between %s and %s and property_owner=%s and is_started_as_lead=true """,
            (self.date_from, self.date_to, self.developer.id))
        leads = self.env.cr.dictfetchone()
        # leads=None

        self.env.cr.execute(""" select count(*) as total_opportunity from crm_lead where  date_open between %s and %s and property_owner=%s and type='opportunity'
            """, (self.date_from, self.date_to, self.developer.id))
        opportunity = self.env.cr.dictfetchone()
        # opportunity = None

        # get
        self.env.cr.execute(""" select mat.name as activity_type,count(*) as cnt from crm_lead c inner join mail_activity ma on c.id=ma.res_id inner join mail_activity_type mat on ma.activity_type_id=mat.id
                                where  ma.date_deadline between %s and %s
                                group by mat.name""", (self.date_from, self.date_to))
        activity_types = self.env.cr.dictfetchall()
        # activity_types = []

        # won
        self.env.cr.execute("""select count(*) as total_won from crm_lead cl inner join crm_stage cs on cl.stage_id=cs.id
                                where cl.date_open between %s and %s and cl.property_owner=%s  and type='opportunity' and cs.is_won=true""",
                            (self.date_from, self.date_to, self.developer.id))
        closed_deals = self.env.cr.dictfetchone()
        # closed_deals = None

        self.env.cr.execute("""select b.plan_type,sum(b.target_number) as target_number from crm_action_plan a 
                                inner join crm_action_plan_detail b 
                                on a.id=action_plan_id where a.date_from>=%s and a.date_to<=%s and a.real_estate=%s group by b.plan_Type""",
                            (self.date_from, self.date_to, self.developer.id))

        plans = self.env.cr.dictfetchall()

        office_visit = 0
        site_visit = 0
        site_office_visit = 0
        inbound_call = 0
        outbound_call = 0

        for activity_type in activity_types:
            if activity_type["activity_type"] == "Office Visit":
                office_visit = activity_type["cnt"]
            elif activity_type["activity_type"] == "Site Visit":
                site_visit = activity_type["cnt"]
            elif activity_type["activity_type"] == "Office and Site Visit":
                site_office_visit = activity_type["cnt"]
            elif activity_type["activity_type"] == "Inbound Call":
                inbound_call = activity_type["cnt"]
            elif activity_type["activity_type"] == "Outbound Call":
                outbound_call = activity_type["cnt"]

        plan_sale = 0
        plan_office_visit = 0
        plan_site_visit = 0
        plan_outbound_call = 0
        plan_lead = 0

        for plan in plans:
            if plan['plan_type'] == 'Sale':
                plan_sale = plan['target_number']
            elif plan['plan_type'] == 'Office Visit':
                plan_office_visit = plan['target_number']
            elif plan['plan_type'] == 'Outgoing Calls':
                plan_outbound_call = plan['target_number']
            elif plan['plan_type'] == 'Lead':
                plan_lead = plan['target_number']
            elif plan['plan_type'] == 'Site Visit':
                plan_site_visit = plan['target_number']

        # lead generation plan
        self.env.cr.execute("""
                        select x.name,x.plan,x.actual,x.actual-x.plan as variation_no,cast(((x.actual*1.00-x.plan*1.00)/x.plan*1.00)*100 as float) as variation_percent,
                ((x.actual*1.00-x.plan*1.00)/x.plan*1.00)*100+100 as sucess_percent,10.00 as lead_result from (
                select a.user_id,(select distinct name from hr_employee where id=a.employee) as name,b.plan_type,b.target_number as plan,
                (select count(*) from crm_lead where user_id=a.user_id and date_open between %s and %s and is_started_as_lead=true) as actual
                from crm_action_plan a
                inner join crm_action_plan_detail b on a.id=action_plan_id
                where a.date_from>=%s and a.date_to<=%s  and b.plan_type='Lead' and a.real_estate=%s
                )x""", (self.date_from, self.date_to, self.date_from, self.date_to, self.developer.id))
        lead_plan_generation = self.env.cr.dictfetchall()

        docargs = {
            'doc_ids': self.ids,
            'developer': self.developer.name,
            'date_from': self.date_from.strftime('%d-%b-%Y'),
            'date_to': self.date_to.strftime('%d-%b-%Y'),
            'leads': leads['total_leads'] if leads is not None else 0,
            'opportunity': opportunity['total_opportunity'] if opportunity is not None else 0,
            'office_visit': office_visit,
            'site_visit': site_visit,
            'office_and_site': office_visit + site_visit,
            'site_office_visit': site_office_visit,
            'inbound_call': inbound_call,
            'outbound_call': outbound_call,
            'closed_deals': closed_deals['total_won'] if closed_deals is not None else 0,
            'plan_sale': plan_sale,
            'plan_office_visit': plan_office_visit,
            'plan_site_visit': plan_site_visit,
            'plan_outbound_call': plan_outbound_call,
            'plan_lead': plan_lead,
            'plan_office_site': plan_office_visit + plan_site_visit,
            'lead_plan_generation': lead_plan_generation

        }

        return self.env.ref('champion_crm.crm_action_performance_report').report_action(tests, docargs)

    def action_get_sales_xls_report(self):

        # This generates our excel file he
        file_io = BytesIO()
        workbook = xlsxwriter.Workbook(file_io)
        self.generate_sales_report_dynamic(workbook)
        self.generate_champion_commission_report(workbook)
        self.generate_sales_commission_report(workbook)
        workbook.close()

        # The file to download will be stored under fileout field
        self.fileout = encodebytes(file_io.getvalue())
        file_io.close()

        # The file name is stored under filename
        datetime_string = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        filename = '%s_%s' % ('Sales Report ', datetime_string)

        # This downloads file. The file is fileout and the name if filename
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'web/content/?model=' + self._name + '&id=' + str(
                self.id) + '&field=fileout&download=true&filename=' + filename,
        }

    def generate_sales_report_dynamic(self, workbook):

        sheet = workbook.add_worksheet('Sales Report')

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 30)
        sheet.set_column('G:G', 30)
        sheet.set_column('H:H', 30)
        sheet.set_column('I:I', 30)
        sheet.set_column('J:J', 30)
        sheet.set_column('K:K', 30)
        sheet.set_column('L:L', 30)
        sheet.set_column('M:M', 30)
        sheet.set_column('N:NZ', 20)

        row_start = 0
        date_format = workbook.add_format(
            {'num_format': 'd mmm yyyy', 'border': 7})
        num_format = workbook.add_format({'num_format': 43, 'border': 7})
        num_format_exchange = workbook.add_format({'num_format': '#,##0.0000', 'border': 7})
        cent_format = workbook.add_format({'num_format': 41, 'border': 7})
        border = workbook.add_format({'border': 7})
        bold = workbook.add_format({'bold': True})
        header_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 22})
        main_title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16})
        parameter_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#F6F5F5'})

        separator_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#D9D9D9'})

        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format1 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format_num = workbook.add_format({
            'bold': 1,
            'border': 1,
            'num_format': 43,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})

        # search RFQ
        sale = self.env['crm.sales'].search([('property_site', '=', self.site.id)], limit=1)

        if sale:
            sales_id = sale.id

            # get each steps
            steps = self.env['crm.construction.progress'].search([('sales_id', '=', sales_id)])

            sheet.merge_range('A' + str(row_start + 1) + ':G' +
                              str(row_start + 1), self.developer.name, header_format)
            sheet.merge_range('A' + str(row_start + 2) + ':G' +
                              str(row_start + 2), self.site.name, header_format)
            sheet.merge_range('A' + str(row_start + 3) + ':G' +
                              str(row_start + 3), 'Sales Report', main_title_format)

            # Define the start column (change this as needed)
            col_start = 9  # Column 'I'

            for step in steps:
                col_start_letter = self.col_num_to_letter(col_start)
                col_end_letter = self.col_num_to_letter(col_start + 5)  # Merge 6 columns
                cell_range = f'{col_start_letter}{row_start + 4}:{col_end_letter}{row_start + 4}'
                sheet.merge_range(cell_range, step['display_name'], title_format1)

                # Write the six columns next to the merged columns
                sheet.write(row_start + 4, col_start - 1, step['display_name'] + ' %', title_format)
                sheet.write(row_start + 4, col_start, 'Expected Payment', title_format)
                sheet.write(row_start + 4, col_start + 1, 'Paid Amount USD', title_format)
                sheet.write(row_start + 4, col_start + 2, 'Paid Amount ETB', title_format)
                sheet.write(row_start + 4, col_start + 3, 'Remaining Balance USD', title_format)
                sheet.write(row_start + 4, col_start + 4, 'Remaining Balance %', title_format)

                col_start += 6  # Move to the next set of 6 columns

            # Write the six columns next to the merged columns
            col_start_letter = self.col_num_to_letter(col_start)
            col_end_letter = self.col_num_to_letter(col_start + 4)  # Merge 5 columns
            cell_range = f'{col_start_letter}{row_start + 4}:{col_end_letter}{row_start + 4}'
            sheet.merge_range(cell_range, "Summary", title_format1)

            sheet.write(row_start + 4, col_start - 1, 'Total Paid Amount in %', title_format)
            sheet.write(row_start + 4, col_start, 'Total Paid Amount in USD', title_format)
            sheet.write(row_start + 4, col_start + 1, 'Total Paid in ETB', title_format)
            sheet.write(row_start + 4, col_start + 2, 'Remaining Balance in %', title_format)
            sheet.write(row_start + 4, col_start + 3, 'Remaining Balance in USD', title_format)

            row_start = 4

            sheet.write(row_start, 0, 'S.No', title_format)
            sheet.write(row_start, 1, 'Client Name', title_format)
            sheet.write(row_start, 2, 'Contract No', title_format)
            sheet.write(row_start, 3, 'Type', title_format)
            sheet.write(row_start, 4, 'Total Gross Area', title_format)
            sheet.write(row_start, 5, 'Floor No', title_format)
            sheet.write(row_start, 6, 'Apartment ID No', title_format)
            sheet.write(row_start, 7, 'Total Contract Amount - USD', title_format)

            # search RFQ
            sales = self.env['crm.sales'].search([('property_site', '=', self.site.id)], order='contract_no')

            row_start = 5
            for sale in sales:
                sheet.write(row_start, 0, row_start - 4, border)
                sheet.write(row_start, 1, sale.customer.name, border)
                sheet.write(row_start, 2, sale.contract_no, border)
                sheet.write(row_start, 3, sale.type, border)
                sheet.write(row_start, 4, sale.size, border)
                sheet.write(row_start, 5, sale.floor, border)
                sheet.write(row_start, 6, "", border)
                sheet.write(row_start, 7, sale.total_usd_with_vat, num_format)

                payment_percent_total = 0
                paid_amount_usd_total = 0
                paid_amount_etb_total = 0
                rem_balance_usd_total = 0
                rem_balance_percent_total = 0

                column_start = 8
                for step in steps:
                    sales_commission = sale.champion_commission.search(
                        [('payment', '=', step.name.id), ('sales_id', '=', sale.id)])
                    construction_progress = sale.construction_progress.search(
                        [('name', '=', step.name.id), ('sales_id', '=', sale.id)])

                    payment_percent = sales_commission.payment_pct if sales_commission.payment_pct else 0
                    expected_payment = self.get_expected_payment_usd(construction_progress)
                    paid_amount_usd = self.get_paid_usd(construction_progress)
                    paid_amount_etb = self.get_payment_etb(construction_progress)
                    rem_balance_usd = expected_payment - paid_amount_usd
                    rem_balance_percent = payment_percent - ((paid_amount_usd * 100) / sale.total_usd_with_vat)

                    payment_percent_total += payment_percent
                    paid_amount_usd_total += paid_amount_usd
                    paid_amount_etb_total += paid_amount_etb
                    rem_balance_usd_total += rem_balance_usd
                    rem_balance_percent_total += rem_balance_percent

                    sheet.write(row_start, column_start, payment_percent, num_format)
                    sheet.write(row_start, column_start + 1, expected_payment, num_format)
                    sheet.write(row_start, column_start + 2, paid_amount_usd, num_format)
                    sheet.write(row_start, column_start + 3, paid_amount_etb, num_format)
                    sheet.write(row_start, column_start + 4, rem_balance_usd, num_format)
                    sheet.write(row_start, column_start + 5, rem_balance_percent, num_format)
                    column_start += 6

                sheet.write(row_start, col_start - 1, payment_percent_total - rem_balance_percent_total, num_format)
                sheet.write(row_start, col_start, paid_amount_usd_total, num_format)
                sheet.write(row_start, col_start + 1, paid_amount_etb_total, num_format)
                sheet.write(row_start, col_start + 2, rem_balance_percent_total, num_format)
                sheet.write(row_start, col_start + 3, rem_balance_usd_total, num_format)

                row_start += 1

    def generate_champion_commission_report(self, workbook):
        sheet = workbook.add_worksheet('Champion Commission')

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 20)
        sheet.set_column('F:F', 30)
        sheet.set_column('G:G', 30)
        sheet.set_column('H:H', 30)
        sheet.set_column('I:I', 30)
        sheet.set_column('J:J', 30)
        sheet.set_column('K:K', 30)
        sheet.set_column('L:L', 30)
        sheet.set_column('M:M', 30)
        sheet.set_column('N:N', 20)

        row_start = 0
        date_format = workbook.add_format(
            {'num_format': 'd mmm yyyy', 'border': 7})
        num_format = workbook.add_format({'num_format': 43, 'border': 7})
        num_format_exchange = workbook.add_format({'num_format': '#,##0.0000', 'border': 7})
        cent_format = workbook.add_format({'num_format': 41, 'border': 7})
        border = workbook.add_format({'border': 7})
        bold = workbook.add_format({'bold': True})
        header_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 22})
        main_title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16})
        parameter_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#F6F5F5'})

        separator_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#D9D9D9'})

        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format1 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format_num = workbook.add_format({
            'bold': 1,
            'border': 1,
            'num_format': 43,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})

        # search RFQ
        sale = self.env['crm.sales'].search([('property_site', '=', self.site.id)], limit=1)

        if sale:
            sales_id = sale.id

            # get each steps
            steps = self.env['crm.sales.commission'].search([('sales_id', '=', sales_id), ('champ_pct', '!=', 0)])

            sheet.merge_range('A' + str(row_start + 1) + ':G' +
                              str(row_start + 1), self.developer.name, header_format)
            sheet.merge_range('A' + str(row_start + 2) + ':G' +
                              str(row_start + 2), self.site.name, header_format)
            sheet.merge_range('A' + str(row_start + 3) + ':G' +
                              str(row_start + 3), 'Champion Commission', main_title_format)

            # Define the start column (change this as needed)
            col_start = 11  # Column 'K'

            for step in steps:
                col_start_letter = self.col_num_to_letter(col_start)
                col_end_letter = self.col_num_to_letter(col_start + 5)  # Merge 6 columns
                cell_range = f'{col_start_letter}{row_start + 4}:{col_end_letter}{row_start + 4}'
                sheet.merge_range(cell_range, step.payment.name, title_format1)

                # Write the six columns next to the merged columns
                sheet.write(row_start + 4, col_start - 1, step.payment.name + ' %', title_format)
                sheet.write(row_start + 4, col_start, 'Payment Amount', title_format)
                sheet.write(row_start + 4, col_start + 1, 'VAT', title_format)
                sheet.write(row_start + 4, col_start + 2, 'Withholding', title_format)
                sheet.write(row_start + 4, col_start + 3, 'Net commission', title_format)
                sheet.write(row_start + 4, col_start + 4, 'Status', title_format)
                col_start += 6  # Move to the next set of 5 columns

            # Write the six columns next to the merged columns
            col_start_letter = self.col_num_to_letter(col_start)
            col_end_letter = self.col_num_to_letter(col_start + 2)  # Merge 5 columns
            cell_range = f'{col_start_letter}{row_start + 4}:{col_end_letter}{row_start + 4}'
            sheet.merge_range(cell_range, "Summary", title_format1)

            sheet.write(row_start + 4, col_start - 1, 'Total Vat Amount', title_format)
            sheet.write(row_start + 4, col_start, 'Total Withholding Amount', title_format)
            sheet.write(row_start + 4, col_start + 1, 'Total Commission Amount', title_format)

            row_start = 4

            sheet.write(row_start, 0, 'S.No', title_format)
            sheet.write(row_start, 1, 'Client Name', title_format)
            sheet.write(row_start, 2, 'Contract No', title_format)
            sheet.write(row_start, 3, 'Type', title_format)
            sheet.write(row_start, 4, 'Total Gross Area', title_format)
            sheet.write(row_start, 5, 'Floor No', title_format)
            sheet.write(row_start, 6, 'Apartment ID No', title_format)
            sheet.write(row_start, 7, 'Total Contract Amount - USD', title_format)
            sheet.write(row_start, 8, 'Total Commission in %', title_format)
            sheet.write(row_start, 9, 'Total Commission Amount ', title_format)

            # search RFQ
            sales = self.env['crm.sales'].search([('property_site', '=', self.site.id)], order='contract_no')

            row_start = 5
            for sale in sales:
                sheet.write(row_start, 0, row_start - 4, border)
                sheet.write(row_start, 1, sale.customer.name, border)
                sheet.write(row_start, 2, sale.contract_no, border)
                sheet.write(row_start, 3, sale.type, border)
                sheet.write(row_start, 4, sale.size, border)
                sheet.write(row_start, 5, sale.floor, border)
                sheet.write(row_start, 6, "", border)
                sheet.write(row_start, 7, sale.total_usd_with_vat, num_format)
                sheet.write(row_start, 8, sale.champ_commission_pct, num_format)
                sheet.write(row_start, 9, sale.champ_commission_amt, num_format)

                total_vat_amount = 0
                total_withholding_amount = 0
                total_commission_amount = 0

                column_start = 10
                for step in steps:
                    sales_commission = sale.champion_commission.search(
                        [('payment', '=', step.payment.id), ('sales_id', '=', sale.id)], limit=1)
                    construction_progress = sale.construction_progress.search(
                        [('name', '=', step.payment.id), ('sales_id', '=', sale.id)])

                    payment_percent = sales_commission.champ_pct if sales_commission.champ_pct else 0
                    commission_amount = sales_commission.commission_claim_total
                    vat_amount = sales_commission.vat_amount
                    with_holding_tax = sales_commission.with_holding_tax
                    net_commission = commission_amount - with_holding_tax
                    if sales_commission.is_commission_paid == "Paid":
                        status = "Collected"
                    else:
                        status = "Not Collected"

                    # total amount
                    total_vat_amount += vat_amount
                    total_withholding_amount += with_holding_tax
                    total_commission_amount += net_commission

                    sheet.write(row_start, column_start, payment_percent, num_format)
                    sheet.write(row_start, column_start + 1, commission_amount, num_format)
                    sheet.write(row_start, column_start + 2, vat_amount, num_format)
                    sheet.write(row_start, column_start + 3, with_holding_tax, num_format)
                    sheet.write(row_start, column_start + 4, net_commission, num_format)
                    sheet.write(row_start, column_start + 5, status, num_format)
                    column_start += 6

                sheet.write(row_start, col_start - 1, total_vat_amount, num_format)
                sheet.write(row_start, col_start, total_withholding_amount, num_format)
                sheet.write(row_start, col_start + 1, total_commission_amount, num_format)

                row_start += 1

    def generate_sales_commission_report(self, workbook):
        sheet = workbook.add_worksheet('Sales Commission')

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 30)
        sheet.set_column('C:C', 30)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:EZ', 20)
        sheet.set_column('F:F', 30)
        sheet.set_column('G:G', 30)
        sheet.set_column('H:H', 30)
        sheet.set_column('I:I', 30)
        sheet.set_column('J:J', 30)
        sheet.set_column('K:K', 30)
        sheet.set_column('L:L', 30)
        sheet.set_column('M:M', 30)
        sheet.set_column('N:NZ', 20)

        row_start = 0
        date_format = workbook.add_format(
            {'num_format': 'd mmm yyyy', 'border': 7})
        num_format = workbook.add_format({'num_format': 43, 'border': 7})
        num_format_exchange = workbook.add_format({'num_format': '#,##0.0000', 'border': 7})
        cent_format = workbook.add_format({'num_format': 41, 'border': 7})
        border = workbook.add_format({'border': 7})
        bold = workbook.add_format({'bold': True})
        header_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 22})
        main_title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16})
        parameter_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#F6F5F5'})

        separator_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#D9D9D9'})

        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format1 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format_num = workbook.add_format({
            'bold': 1,
            'border': 1,
            'num_format': 43,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})

        # search RFQ
        sale = self.env['crm.sales'].search([('property_site', '=', self.site.id)], limit=1)

        if sale:
            sales_id = sale.id

            # get each steps
            steps = self.env['crm.sales.commission'].search([('sales_id', '=', sales_id), ('sales_agent_pct', '!=', 0)])

            sheet.merge_range('A' + str(row_start + 1) + ':G' +
                              str(row_start + 1), self.developer.name, header_format)
            sheet.merge_range('A' + str(row_start + 2) + ':G' +
                              str(row_start + 2), self.site.name, header_format)
            sheet.merge_range('A' + str(row_start + 3) + ':G' +
                              str(row_start + 3), 'Sales Commission', main_title_format)

            # Define the start column (change this as needed)
            col_start = 11  # Column 'K'

            for step in steps:
                col_start_letter = self.col_num_to_letter(col_start)
                col_end_letter = self.col_num_to_letter(col_start + 4)  # Merge 6 columns
                cell_range = f'{col_start_letter}{row_start + 4}:{col_end_letter}{row_start + 4}'
                sheet.merge_range(cell_range, step.payment.name, title_format1)

                # Write the six columns next to the merged columns
                sheet.write(row_start + 4, col_start - 1, step.payment.name + ' %', title_format)
                sheet.write(row_start + 4, col_start, 'Amount', title_format)
                sheet.write(row_start + 4, col_start + 1, 'Tax', title_format)
                sheet.write(row_start + 4, col_start + 2, 'Paid Amount', title_format)
                sheet.write(row_start + 4, col_start + 3, 'Status', title_format)
                col_start += 5  # Move to the next set of 5 columns

            # Write the six columns next to the merged columns
            col_start_letter = self.col_num_to_letter(col_start)
            col_end_letter = self.col_num_to_letter(col_start + 2)  # Merge 5 columns
            cell_range = f'{col_start_letter}{row_start + 4}:{col_end_letter}{row_start + 4}'
            sheet.merge_range(cell_range, "Summary", title_format1)

            sheet.write(row_start + 4, col_start - 1, 'Total Commission Amount', title_format)
            sheet.write(row_start + 4, col_start, 'Total Tax Amount', title_format)
            sheet.write(row_start + 4, col_start + 1, 'Net Commission Amount', title_format)

            row_start = 4

            sheet.write(row_start, 0, 'S.No', title_format)
            sheet.write(row_start, 1, 'Client Name', title_format)
            sheet.write(row_start, 2, 'Contract No', title_format)
            sheet.write(row_start, 3, 'Type', title_format)
            sheet.write(row_start, 4, 'Total Gross Area', title_format)
            sheet.write(row_start, 5, 'Floor No', title_format)
            sheet.write(row_start, 6, 'Apartment ID No', title_format)
            sheet.write(row_start, 7, 'Total Contract Amount - USD', title_format)
            sheet.write(row_start, 8, 'Total Commission in %', title_format)
            sheet.write(row_start, 9, 'Total Commission Amount ', title_format)

            # search RFQ
            sales = self.env['crm.sales'].search([('property_site', '=', self.site.id)], order='contract_no')

            row_start = 5
            for sale in sales:
                sheet.write(row_start, 0, row_start - 4, border)
                sheet.write(row_start, 1, sale.customer.name, border)
                sheet.write(row_start, 2, sale.contract_no, border)
                sheet.write(row_start, 3, sale.type, border)
                sheet.write(row_start, 4, sale.size, border)
                sheet.write(row_start, 5, sale.floor, border)
                sheet.write(row_start, 6, "", border)
                sheet.write(row_start, 7, sale.total_usd_with_vat, num_format)
                sheet.write(row_start, 8, sale.agent_commission_pct, num_format)
                sheet.write(row_start, 9, sale.agent_commission_amt, num_format)

                tax_amount_total = 0
                commission_total = 0
                net_commission_total = 0

                column_start = 10
                for step in steps:
                    sales_commission = sale.champion_commission.search(
                        [('payment', '=', step.payment.id), ('sales_id', '=', sale.id)], limit=1)
                    sale.construction_progress.search(
                        [('name', '=', step.payment.id), ('sales_id', '=', sale.id)])

                    payment_percent = sales_commission.sales_agent_pct if sales_commission.sales_agent_pct else 0
                    commission_amount = sales_commission.sales_agnet_commission
                    tax_amount = self.get_sales_commission_tax(sale)
                    net_commission = commission_amount - tax_amount
                    if sales_commission.is_commission_paid == "Paid":
                        status = "Paid"
                    else:
                        status = "Not Paid"

                    tax_amount_total += tax_amount
                    commission_total += commission_amount
                    net_commission_total += net_commission

                    sheet.write(row_start, column_start, payment_percent, num_format)
                    sheet.write(row_start, column_start + 1, commission_amount, num_format)
                    sheet.write(row_start, column_start + 2, tax_amount, num_format)
                    sheet.write(row_start, column_start + 3, net_commission, num_format)
                    sheet.write(row_start, column_start + 4, status, num_format)
                    column_start += 5

                sheet.write(row_start, col_start - 1, commission_total, num_format)
                sheet.write(row_start, col_start, tax_amount_total, num_format)
                sheet.write(row_start, col_start + 1, net_commission_total, num_format)

                row_start += 1

    def get_expected_payment_usd(self, construction_progress):
        total_amount = 0
        for record in construction_progress:
            total_amount += record.payment_amount_USD

        return total_amount

    def get_paid_usd(self, construction_progress):
        total_amount = 0
        for record in construction_progress:
            for line in record.payments:
                total_amount += line.paid_usd

        return total_amount

    def get_payment_etb(self, construction_progress):
        total_amount = 0
        for record in construction_progress:
            for line in record.payments:
                total_amount += line.payment_amount

        return total_amount

    def get_sales_commission_tax(self, sales):
        total_tax_amount = 0
        for record in sales:
            for commissions in record.champion_commission:
                for line in commissions.sales_agent_commision_ids:
                    total_tax_amount += line.tax_amount

        return total_tax_amount

    def action_get_sales_summary_xls_report(self):

        # This generates our excel file he
        file_io = BytesIO()
        workbook = xlsxwriter.Workbook(file_io)
        self.generate_weekly_sales_report(workbook)
        self.generate_weekly_sales_team_report(workbook)
        self.generate_weekly_sales_team_graph_report(workbook)
        workbook.close()

        # The file to download will be stored under fileout field
        self.fileout = encodebytes(file_io.getvalue())
        file_io.close()

        # The file name is stored under filename
        datetime_string = datetime.datetime.now().strftime("%Y%m%d_%H%M%S")
        filename = '%s_%s' % ('Sales Report ', datetime_string)

        # This downloads file. The file is fileout and the name if filename
        return {
            'type': 'ir.actions.act_url',
            'target': 'new',
            'url': 'web/content/?model=' + self._name + '&id=' + str(
                self.id) + '&field=fileout&download=true&filename=' + filename,
        }

    def generate_weekly_sales_report(self, workbook):
        sheet = workbook.add_worksheet('Sales Weekly Report')

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 5)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 10)
        sheet.set_column('I:I', 10)
        sheet.set_column('J:J', 10)
        sheet.set_column('K:K', 10)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 10)

        row_start = 0
        date_format = workbook.add_format(
            {'num_format': 'd mmm yyyy', 'border': 7})
        num_format = workbook.add_format({'num_format': 43, 'border': 1})
        num_format_bold = workbook.add_format({'num_format': 43, 'border': 1, 'bold': 1})
        percent_format = workbook.add_format({'num_format': '0.00%', 'border': 1})
        num_seq_format = workbook.add_format({'num_format': 0, 'border': 1})
        num_format_exchange = workbook.add_format({'num_format': '#,##0.0000', 'border': 7})
        cent_format = workbook.add_format({'num_format': 41, 'border': 7})
        border = workbook.add_format({'border': 1})
        bold = workbook.add_format({'bold': True})
        header_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 22})
        main_title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16})
        parameter_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#F6F5F5'})

        separator_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#D9D9D9'})

        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format2 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format1 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format_num = workbook.add_format({
            'bold': 1,
            'border': 1,
            'num_format': 43,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})

        sheet.merge_range('A' + str(row_start + 1) + ':L' +
                          str(row_start + 1), 'የሽያጭ የሳምንት የአስራ አምስት ቀን እና የወር ሪፖርት ማጠቃለያ', header_format)
        sheet.merge_range('A' + str(row_start + 2) + ':L' +
                          str(row_start + 2), 'የቅጽ ቁጥር፦ 008', main_title_format)

        query = """ SELECT t.name AS team,rp.name AS user,mat.name AS activity,COUNT(*) AS count FROM crm_lead cl
                INNER JOIN crm_activity_report car ON cl.id = car.lead_id INNER JOIN crm_team t ON cl.team_id = t.id
                INNER JOIN res_users u ON cl.user_id = u.id INNER JOIN res_partner rp ON u.partner_id = rp.id  
                INNER JOIN mail_activity_type mat ON car.mail_activity_type_id = mat.id
                WHERE cl.team_id IS NOT NULL AND cl.user_id IS NOT null AND car.date BETWEEN '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                GROUP BY cl.team_id, t.name, cl.user_id, rp.name,  car.mail_activity_type_id, mat.name
      
                union all              
                   
                SELECT t.name AS team,rp.name AS user,'Lead' AS activity,COUNT(*) AS count FROM  crm_lead cl
                INNER JOIN crm_team t ON cl.team_id = t.id INNER JOIN res_users u ON cl.user_id = u.id INNER JOIN 
                res_partner rp ON u.partner_id = rp.id                  
                WHERE  cl.team_id IS NOT NULL AND cl.user_id IS NOT null AND cl.date_open BETWEEN '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                GROUP BY cl.team_id, t.name, cl.user_id, rp.name;
               
;                   """

        self.env.cr.execute(query)
        result = self.env.cr.dictfetchall()

        df = pd.DataFrame(result)

        # Pivot the DataFrame to convert mail_activity_id to individual columns and sum the count column
        pivot_df = df.pivot_table(index=['team', 'user'], columns='activity', values='count',
                                  aggfunc='sum', fill_value=0).reset_index()

        # Convert pivot table to dictionary
        pivot_dict = pivot_df.to_dict(orient='index')

        # Define the start column (change this as needed)
        col_start = 1  # Column 'B'

        # Write the six columns next to the merged columns
        # sheet.write(row_start + 2, col_start, 'ተ.ቁ', title_format)
        # sheet.write(row_start + 2, col_start+1, 'የሽያጭ ባለሙያው ስም', title_format)

        sheet.merge_range('B3:B4', 'ተ.ቁ', title_format)
        sheet.merge_range('C3:C4', 'የሽያጭ ባለሙያው ስም', title_format)

        sheet.merge_range('D3:E3', 'የአዲስ ደንበኛ ቁጥር', title_format)
        sheet.write("D4", 'ዕቅድ', title_format)
        sheet.write("E4", 'አፈፃፀም', title_format)

        sheet.merge_range('F3:G3', 'የተደወሉ ስልኮች ቁጥር', title_format)
        sheet.write("F4", 'ዕቅድ', title_format)
        sheet.write("G4", 'አፈፃፀም', title_format)

        sheet.merge_range('H3:I3', 'የሳይት እና የቢሮ ጉብኝቶች ቁጥር', title_format)
        sheet.write("H4", 'ዕቅድ', title_format)
        sheet.write("I4", 'አፈፃፀም', title_format)

        sheet.merge_range('J3:K3', 'የሽያጭ ቁጥር', title_format)
        sheet.write("J4", 'ዕቅድ', title_format)
        sheet.write("K4", 'አፈፃፀም', title_format)

        sheet.merge_range('L3:L4', 'ውጤት', title_format)

        new_team = ''
        team_count = 0

        lead_plan = call_plan = visit_plan = sales_plan = 1

        lead_plan_total = lead_result_total = call_plan_total = call_total = visit_plan_total = visit_total = sales_plan_total = sales_total = result_total = 0

        row_start = 4
        no = 1
        team_user_count = 0
        for key in pivot_dict:
            if new_team == '':
                new_team = pivot_dict[key]['team']
                # create new group
                merge_rows = 'B' + str(row_start + 1) + ':L' + str(row_start + 1)
                sheet.merge_range(merge_rows, pivot_dict[key]['team'], title_format2)
                row_start += 1
                team_count += 1
            elif new_team != pivot_dict[key]['team']:
                merge_rows = 'B' + str(row_start + 1) + ':C' + str(row_start + 1)
                sheet.merge_range(merge_rows, new_team + ' Total', title_format2)

                sheet.write(row_start, 3, lead_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 4, lead_result_total, num_format_bold)

                sheet.write(row_start, 5, call_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 6, call_total, num_format_bold)

                sheet.write(row_start, 7, visit_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 8, visit_total, num_format_bold)

                sheet.write(row_start, 9, sales_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 10, sales_total, num_format_bold)

                sheet.write(row_start, 11, result_total / team_user_count, percent_format)

                team_user_count = 0

                lead_plan_total = lead_result_total = call_plan_total = call_total = visit_plan_total = visit_total = sales_plan_total = sales_total = result_total = 0

                new_team = pivot_dict[key]['team']
                merge_rows = 'B' + str(row_start + 2) + ':L' + str(row_start + 2)
                sheet.merge_range(merge_rows, pivot_dict[key]['team'], title_format2)
                row_start += 2
                team_count += 1

            # get action plan
            action_plans = self.env["crm.action.plan"].search([('employee.user_id', '=', pivot_dict[key]['user'])])

            for action_plan in action_plans:
                lead_plan = 0
                call_plan = 0
                visit_plan = 0
                sales_plan = 0
                for plan in action_plan.action_details:
                    if plan.plan_type == 'Lead':
                        lead_plan += int(plan.target_number)
                    elif plan.plan_type == 'Site Visit' or plan.plan_type == 'Office Visit':
                        visit_plan += int(plan.target_number)
                    elif plan.plan_type == 'Outgoing Calls':
                        call_plan += int(plan.target_number)
                    elif plan.plan_type == 'Sale':
                        sales_plan += int(plan.target_number)

            sheet.write(row_start, 1, no, num_seq_format)
            sheet.write(row_start, 2, pivot_dict[key]['user'], num_format)

            sheet.write(row_start, 3, lead_plan, num_format)  # plan
            sheet.write(row_start, 4, pivot_dict[key]['Lead'], num_format)

            call = 0

            lead_result = float(pivot_dict[key]['Lead'])

            if 'Outbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Outbound Call'])
            if 'Inbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Inbound Call'])

            sheet.write(row_start, 5, call_plan, num_format)  # plan
            sheet.write(row_start, 6, call, num_format)

            visit = 0
            if 'Office Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Office Visit'])
            if 'Site Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Site Visit'])

            sheet.write(row_start, 7, visit_plan, num_format)  # plan
            sheet.write(row_start, 8, visit, num_format)

            sales = 1
            if 'Closed' in pivot_dict[key]:
                sales += float(pivot_dict[key]['Closed'])

            sheet.write(row_start, 9, sales_plan, num_format)  # plan
            sheet.write(row_start, 10, sales, num_format)

            # result

            result = (lead_result / lead_plan) * 0.1 + (call / call_plan) * 0.15 + (visit / visit_plan) * 0.25 + (
                    sales_plan / sales) * 0.5
            sheet.write(row_start, 11, result, percent_format)

            lead_plan_total += lead_plan
            lead_result_total += lead_result

            call_plan_total += call_plan
            call_total += call

            visit_plan_total += visit_plan
            visit_total += visit

            sales_plan_total += sales_plan
            sales_total += sales

            result_total += result

            row_start += 1
            no += 1
            team_user_count += 1

        merge_rows = 'B' + str(row_start + 1) + ':C' + str(row_start + 1)
        sheet.merge_range(merge_rows, new_team + ' Total', title_format2)

        sheet.write(row_start, 3, lead_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 4, lead_result_total, num_format_bold)

        sheet.write(row_start, 5, call_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 6, call_total, num_format_bold)

        sheet.write(row_start, 7, visit_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 8, visit_total, num_format_bold)

        sheet.write(row_start, 9, sales_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 10, sales_total, num_format_bold)

        sheet.write(row_start, 11, result_total / team_user_count, percent_format)

    def generate_weekly_sales_team_report(self, workbook):
        sheet = workbook.add_worksheet('Sales Team Weekly Report')

        # region formats
        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 5)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 20)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 15)
        sheet.set_column('G:G', 15)
        sheet.set_column('H:H', 10)

        sheet.set_column('I:I', 5)
        sheet.set_column('J:J', 20)
        sheet.set_column('K:K', 20)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:M', 15)
        sheet.set_column('N:N', 15)

        row_start = 0

        num_format = workbook.add_format({'num_format': 43, 'border': 1})
        num_format_bold = workbook.add_format({'num_format': 1, 'border': 1, 'bold': 1})
        percent_format = workbook.add_format({'num_format': '0.00%', 'border': 1})
        header_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 22})
        main_title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16})
        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})

        # endregion

        # region sales by teams

        sheet.merge_range('A' + str(row_start + 1) + ':L' +
                          str(row_start + 1), 'የሽያጭ የሳምንት የአስራ አምስት ቀን እና የወር  የደረጃ ማጠቃለያ', header_format)
        sheet.merge_range('A' + str(row_start + 2) + ':L' +
                          str(row_start + 2), 'የቅጽ ቁጥር፦ 009', main_title_format)

        query = """ SELECT t.name AS team,rp.name AS user,mat.name AS activity,COUNT(*) AS count FROM crm_lead cl
                    INNER JOIN crm_activity_report car ON cl.id = car.lead_id INNER JOIN crm_team t ON cl.team_id = t.id
                    INNER JOIN res_users u ON cl.user_id = u.id INNER JOIN res_partner rp ON u.partner_id = rp.id  
                    INNER JOIN mail_activity_type mat ON car.mail_activity_type_id = mat.id
                    WHERE cl.team_id IS NOT NULL AND cl.user_id IS NOT null AND car.date BETWEEN '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                    GROUP BY cl.team_id, t.name, cl.user_id, rp.name,  car.mail_activity_type_id, mat.name

                    union all              

                    SELECT t.name AS team,rp.name AS user,'Lead' AS activity,COUNT(*) AS count FROM  crm_lead cl
                    INNER JOIN crm_team t ON cl.team_id = t.id INNER JOIN res_users u ON cl.user_id = u.id INNER JOIN 
                    res_partner rp ON u.partner_id = rp.id                  
                    WHERE  cl.team_id IS NOT NULL AND cl.user_id IS NOT null AND cl.date_open BETWEEN '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                    GROUP BY cl.team_id, t.name, cl.user_id, rp.name;

    ;                   """

        self.env.cr.execute(query)
        result = self.env.cr.dictfetchall()

        df = pd.DataFrame(result)

        # Pivot the DataFrame to convert mail_activity_id to individual columns and sum the count column
        pivot_df = df.pivot_table(index=['team', 'user'], columns='activity', values='count',
                                  aggfunc='sum', fill_value=0).reset_index()

        # Convert pivot table to dictionary
        pivot_dict = pivot_df.to_dict(orient='index')
        sheet.merge_range('B4:G4', 'የቡድን ውጤት', title_format)
        sheet.write(4, 1, 'ተ.ቁ', title_format)
        sheet.write(4, 2, 'የቡድን ስም', title_format)
        sheet.write(4, 3, 'የቡድን መሪ', title_format)
        sheet.write(4, 4, 'ውጤት', title_format)
        sheet.write(4, 5, 'የአፈፃፀም ደረጃ', title_format)
        sheet.write(4, 6, 'ያለፈው አፈፃፀም ደረጃ', title_format)

        new_team = ''
        lead_plan = call_plan = visit_plan = sales_plan = 1
        lead_plan_total = lead_result_total = call_plan_total = call_total = visit_plan_total = visit_total = sales_plan_total = sales_total = result_total = 0

        # Set to keep track of unique names
        unique_names = set()

        # Loop through the list and add unique names to the set
        for key in pivot_dict:
            unique_names.add(pivot_dict[key]['team'])

        # The length of the set gives the count of unique names
        unique_row_count = len(unique_names)

        row_start = 5
        no = 1
        team_user_count = 0

        column_start_point_no = 6
        column_start_no = 6
        column_end_no = 5 + unique_row_count

        for key in pivot_dict:
            if new_team == '':
                new_team = pivot_dict[key]['team']
            elif new_team != pivot_dict[key]['team']:

                sheet.write(row_start, 1, no, num_format_bold)
                sheet.write(row_start, 2, new_team, num_format)

                sheet.write(row_start, 3, self.get_team_leader(pivot_dict[key]['team']), num_format)
                sheet.write(row_start, 4, result_total / team_user_count, percent_format)

                rank_formula = '=RANK(E' + str(column_start_no) + ',($E$' + str(column_start_point_no) + ':$E$' + str(
                    column_end_no) + '),0)'
                sheet.write_formula(row_start, 5, rank_formula, num_format)
                sheet.write(row_start, 6, ' ', num_format)

                lead_plan_total = lead_result_total = call_plan_total = call_total = visit_plan_total = visit_total = sales_plan_total = sales_total = result_total = 0

                new_team = pivot_dict[key]['team']

                row_start += 1
                no += 1
                team_user_count += 1
                column_start_no += 1

            # get action plan
            action_plans = self.env["crm.action.plan"].search([('employee.user_id', '=', pivot_dict[key]['user'])])

            for action_plan in action_plans:
                lead_plan = 0
                call_plan = 0
                visit_plan = 0
                sales_plan = 0
                for plan in action_plan.action_details:
                    if plan.plan_type == 'Lead':
                        lead_plan += int(plan.target_number)
                    elif plan.plan_type == 'Site Visit' or plan.plan_type == 'Office Visit':
                        visit_plan += int(plan.target_number)
                    elif plan.plan_type == 'Outgoing Calls':
                        call_plan += int(plan.target_number)
                    elif plan.plan_type == 'Sale':
                        sales_plan += int(plan.target_number)

            call = 0

            lead_result = float(pivot_dict[key]['Lead'])

            if 'Outbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Outbound Call'])
            if 'Inbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Inbound Call'])

            visit = 0
            if 'Office Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Office Visit'])
            if 'Site Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Site Visit'])

            sales = 1
            if 'Closed' in pivot_dict[key]:
                sales += float(pivot_dict[key]['Closed'])

            # result

            result = (lead_result / lead_plan) * 0.1 + (call / call_plan) * 0.15 + (visit / visit_plan) * 0.25 + (
                    sales_plan / sales) * 0.5

            lead_plan_total += lead_plan
            lead_result_total += lead_result

            call_plan_total += call_plan
            call_total += call

            visit_plan_total += visit_plan
            visit_total += visit

            sales_plan_total += sales_plan
            sales_total += sales

            result_total += result

            team_user_count += 1

        rank_formula = '=RANK(E' + str(column_start_no) + ',($E$' + str(column_start_point_no) + ':$E$' + str(
            column_end_no) + '),0)'

        sheet.write(row_start, 1, no, num_format_bold)
        sheet.write(row_start, 2, new_team, num_format)

        sheet.write(row_start, 3, self.get_team_leader(pivot_dict[key]['team']), num_format)
        sheet.write(row_start, 4, result_total / team_user_count, percent_format)

        sheet.write_formula(row_start, 5, rank_formula, num_format)
        sheet.write(row_start, 6, ' ', num_format)

        # endregion

        # region sales by supervisor

        # add two row spaces
        row_start += 2

        query1 = """ select ct.name AS team,rp.name AS user,mat.name AS activity,COUNT(*) AS count from crm_lead cl 
                        inner join crm_activity_report car on cl.id=car.lead_id 
                        inner join crm_team ct on ct.user_id =cl.user_id 
                        inner join res_users ru on ru.id =ct.user_id 
                        inner join res_partner rp on ru.partner_id =rp.id 
                        inner join mail_activity_type mat on car.mail_activity_type_id =mat.id 
                        where cl.team_id is not null and cl.user_id is not null and car.date between  '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                        group by cl.team_id ,ct."name" ,cl.user_id ,rp."name" ,car.mail_activity_type_id ,mat."name" 
                        
                        union all
                        
                        select ct.name AS team,rp.name AS user,'Lead' AS activity,COUNT(*) AS count from crm_lead cl 
                        inner join crm_team ct on ct.user_id =cl.user_id 
                        inner join res_users ru on ru.id =ct.user_id 
                        inner join res_partner rp on ru.partner_id =rp.id 
                        where cl.team_id is not null and cl.user_id is not null and cl.date_open between  '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                        group by cl.team_id ,ct."name" ,cl.user_id ,rp."name";"""

        self.env.cr.execute(query1)
        result1 = self.env.cr.dictfetchall()

        df = pd.DataFrame(result1)

        # Pivot the DataFrame to convert mail_activity_id to individual columns and sum the count column
        pivot_df = df.pivot_table(index=['team', 'user'], columns='activity', values='count',
                                  aggfunc='sum', fill_value=0).reset_index()

        # Convert pivot table to dictionary
        pivot_dict = pivot_df.to_dict(orient='index')

        # Set to keep track of unique names
        unique_names = set()

        # Loop through the list and add unique names to the set
        for key in pivot_dict:
            unique_names.add(pivot_dict[key]['team'])

        # The length of the set gives the count of unique names
        unique_row_count = len(unique_names)

        column_start_point_no = row_start + 2
        column_start_no = row_start + 2
        column_end_no = row_start + 1 + unique_row_count

        sheet.write(row_start, 1, 'ተ.ቁ', title_format)
        sheet.write(row_start, 2, 'የሽያጭ ባለሙያው ስም', title_format)
        sheet.write(row_start, 3, 'የስራ ድርሻ', title_format)
        sheet.write(row_start, 4, 'ውጤት', title_format)
        sheet.write(row_start, 5, 'የአፈፃፀም ደረጃ', title_format)
        sheet.write(row_start, 6, 'ያለፈው አፈፃፀም ደረጃ', title_format)

        lead_plan = call_plan = visit_plan = sales_plan = 1
        no = 1

        row_start += 1
        for key in pivot_dict:
            # get action plan
            action_plans = self.env["crm.action.plan"].search([('employee.user_id', '=', pivot_dict[key]['user'])])

            for action_plan in action_plans:
                lead_plan = 0
                call_plan = 0
                visit_plan = 0
                sales_plan = 0
                for plan in action_plan.action_details:
                    if plan.plan_type == 'Lead':
                        lead_plan += int(plan.target_number)
                    elif plan.plan_type == 'Site Visit' or plan.plan_type == 'Office Visit':
                        visit_plan += int(plan.target_number)
                    elif plan.plan_type == 'Outgoing Calls':
                        call_plan += int(plan.target_number)
                    elif plan.plan_type == 'Sale':
                        sales_plan += int(plan.target_number)

            # get results
            lead_result = float(pivot_dict[key]['Lead'])

            call = 0
            if 'Outbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Outbound Call'])
            if 'Inbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Inbound Call'])

            visit = 0
            if 'Office Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Office Visit'])
            if 'Site Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Site Visit'])

            sales = 1
            if 'Closed' in pivot_dict[key]:
                sales += float(pivot_dict[key]['Closed'])

            # result

            result = (lead_result / lead_plan) * 0.1 + (call / call_plan) * 0.15 + (visit / visit_plan) * 0.25 + (
                    sales_plan / sales) * 0.5

            rank_formula = '=RANK(E' + str(column_start_no) + ',($E$' + str(column_start_point_no) + ':$E$' + str(
                column_end_no) + '),0)'

            # display the result
            sheet.write(row_start, 1, no, num_format_bold)
            sheet.write(row_start, 2, pivot_dict[key]['user'], num_format)

            sheet.write(row_start, 3, 'Supervisor', num_format)
            sheet.write(row_start, 4, result, percent_format)

            sheet.write_formula(row_start, 5, rank_formula, num_format)
            sheet.write(row_start, 6, ' ', num_format)

            row_start += 1
            no += 1
            column_start_no += 1

        # endregion

        # region all sales team report and rank
        sheet.merge_range('I4:N4', 'የከፍተኛ የሽያጭ ባለሙያ ውጤት', title_format)
        sheet.write(4, 8, 'ተ.ቁ', title_format)
        sheet.write(4, 9, 'የሽያጭ ባለሙያው ስም', title_format)
        sheet.write(4, 10, 'የቡድን መሪ', title_format)
        sheet.write(4, 11, 'ውጤት', title_format)
        sheet.write(4, 12, 'የአፈፃፀም ደረጃ', title_format)
        sheet.write(4, 13, 'ያለፈው አፈፃፀም ደረጃ', title_format)

        self.env.cr.execute(query)
        result2 = self.env.cr.dictfetchall()

        df = pd.DataFrame(result2)

        # Pivot the DataFrame to convert mail_activity_id to individual columns and sum the count column
        pivot_df = df.pivot_table(index=['team', 'user'], columns='activity', values='count',
                                  aggfunc='sum', fill_value=0).reset_index()

        # Convert pivot table to dictionary
        pivot_dict = pivot_df.to_dict(orient='index')

        # Set to keep track of unique names
        unique_names = set()

        # Loop through the list and add unique names to the set
        for key in pivot_dict:
            unique_names.add(pivot_dict[key]['user'])

        # The length of the set gives the count of unique names
        unique_row_count = len(unique_names)

        row_start = 5
        no = 1
        team_user_count = 0

        column_start_point_no = 6
        column_start_no = 6
        column_end_no = 5 + unique_row_count

        for key in pivot_dict:
            # get action plan
            action_plans = self.env["crm.action.plan"].search([('employee.user_id', '=', pivot_dict[key]['user'])])

            for action_plan in action_plans:
                lead_plan = 0
                call_plan = 0
                visit_plan = 0
                sales_plan = 0
                for plan in action_plan.action_details:
                    if plan.plan_type == 'Lead':
                        lead_plan += int(plan.target_number)
                    elif plan.plan_type == 'Site Visit' or plan.plan_type == 'Office Visit':
                        visit_plan += int(plan.target_number)
                    elif plan.plan_type == 'Outgoing Calls':
                        call_plan += int(plan.target_number)
                    elif plan.plan_type == 'Sale':
                        sales_plan += int(plan.target_number)

            # get results
            lead_result = float(pivot_dict[key]['Lead'])

            call = 0
            if 'Outbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Outbound Call'])
            if 'Inbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Inbound Call'])

            visit = 0
            if 'Office Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Office Visit'])
            if 'Site Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Site Visit'])

            sales = 1
            if 'Closed' in pivot_dict[key]:
                sales += float(pivot_dict[key]['Closed'])

            # result

            result = (lead_result / lead_plan) * 0.1 + (call / call_plan) * 0.15 + (visit / visit_plan) * 0.25 + (
                    sales_plan / sales) * 0.5

            rank_formula = '=RANK(L' + str(column_start_no) + ',($L$' + str(column_start_point_no) + ':$L$' + str(
                column_end_no) + '),0)'

            # display the result
            sheet.write(row_start, 8, no, num_format_bold)
            sheet.write(row_start, 9, pivot_dict[key]['user'], num_format)

            sheet.write(row_start, 10, self.get_team_leader(pivot_dict[key]['team']), num_format)
            sheet.write(row_start, 11, result, percent_format)

            sheet.write_formula(row_start, 12, rank_formula, num_format)
            sheet.write(row_start, 13, ' ', num_format)

            row_start += 1
            no += 1
            column_start_no += 1

        # endregion

    def generate_weekly_sales_team_graph_report(self, workbook):
        sheet = workbook.add_worksheet('Sales Team Weekly Report Graph')

        sheet.set_column('A:A', 5)
        sheet.set_column('B:B', 5)
        sheet.set_column('C:C', 20)
        sheet.set_column('D:D', 10)
        sheet.set_column('E:E', 10)
        sheet.set_column('F:F', 10)
        sheet.set_column('G:G', 10)
        sheet.set_column('H:H', 10)
        sheet.set_column('I:I', 10)
        sheet.set_column('J:J', 10)
        sheet.set_column('K:K', 10)
        sheet.set_column('L:L', 10)
        sheet.set_column('M:M', 10)
        sheet.set_column('N:N', 10)
        sheet.set_column('U:U', 10)

        row_start = 0
        graph_format_blue = workbook.add_format({
            'font_name': 'Playbill',  # Font name
            'font_size': 8,  # Font size
            'bold': True,  # Bold text
            'font_color': '#4F81BD',  # Font color
            'border': 1,
        })
        graph_format_burgundy = workbook.add_format({
            'font_name': 'Playbill',  # Font name
            'font_size': 8,  # Font size
            'bold': True,  # Bold text
            'font_color': '#C0504D',  # Font color
            'border': 1,
        })
        graph_format_purple = workbook.add_format({
            'font_name': 'Playbill',  # Font name
            'font_size': 8,  # Font size
            'bold': True,  # Bold text
            'font_color': '#B1A0C7',  # Font color
            'border': 1,
        })
        graph_format_red = workbook.add_format({
            'font_name': 'Playbill',  # Font name
            'font_size': 8,  # Font size
            'bold': True,  # Bold text
            'font_color': '#FF0000',  # Font color
            'border': 1,
        })
        graph_format_green = workbook.add_format({
            'font_name': 'Playbill',  # Font name
            'font_size': 8,  # Font size
            'bold': True,  # Bold text
            'font_color': '#9BBB59',  # Font color
            'border': 1,
        })
        date_format = workbook.add_format(
            {'num_format': 'd mmm yyyy', 'border': 7})
        num_format = workbook.add_format({'num_format': 43, 'border': 1})
        num_format_bold = workbook.add_format({'num_format': 43, 'border': 1, 'bold': 1})
        percent_format = workbook.add_format({'num_format': '0.00%', 'border': 1})
        num_seq_format = workbook.add_format({'num_format': 0, 'border': 1})
        num_format_exchange = workbook.add_format({'num_format': '#,##0.0000', 'border': 7})
        cent_format = workbook.add_format({'num_format': 41, 'border': 7})
        border = workbook.add_format({'border': 1})
        bold = workbook.add_format({'bold': True})
        header_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 22})
        main_title_format = workbook.add_format({
            'bold': 1,
            'border': 0,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16})
        parameter_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#F6F5F5'})

        separator_format = workbook.add_format({
            'bold': 1,
            'border': 7,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 12,
            'fg_color': '#D9D9D9'})

        title_format = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format2 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'left',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format1 = workbook.add_format({
            'bold': 1,
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 16,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})
        title_format_num = workbook.add_format({
            'bold': 1,
            'border': 1,
            'num_format': 43,
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 11,
            'text_wrap': 1,
            'fg_color': '#F6F5F5'})

        sheet.merge_range('A' + str(row_start + 1) + ':L' +
                          str(row_start + 1), 'የሽያጭ የሳምንት የአስራ አምስት ቀን እና የወር ሪፖርት ማጠቃለያ', header_format)
        sheet.merge_range('A' + str(row_start + 2) + ':L' +
                          str(row_start + 2), 'የቅጽ ቁጥር፦ 008', main_title_format)

        query = """ SELECT t.name AS team,rp.name AS user,mat.name AS activity,COUNT(*) AS count FROM crm_lead cl
                           INNER JOIN crm_activity_report car ON cl.id = car.lead_id INNER JOIN crm_team t ON cl.team_id = t.id
                           INNER JOIN res_users u ON cl.user_id = u.id INNER JOIN res_partner rp ON u.partner_id = rp.id  
                           INNER JOIN mail_activity_type mat ON car.mail_activity_type_id = mat.id
                           WHERE cl.team_id IS NOT NULL AND cl.user_id IS NOT null AND car.date BETWEEN '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                           GROUP BY cl.team_id, t.name, cl.user_id, rp.name,  car.mail_activity_type_id, mat.name
    
                           union all              
    
                           SELECT t.name AS team,rp.name AS user,'Lead' AS activity,COUNT(*) AS count FROM  crm_lead cl
                           INNER JOIN crm_team t ON cl.team_id = t.id INNER JOIN res_users u ON cl.user_id = u.id INNER JOIN 
                           res_partner rp ON u.partner_id = rp.id                  
                           WHERE  cl.team_id IS NOT NULL AND cl.user_id IS NOT null AND cl.date_open BETWEEN '""" + str(
            self.date_from) + """' and '""" + str(self.date_to) + """'
                           GROUP BY cl.team_id, t.name, cl.user_id, rp.name;
    
           ;                   """

        self.env.cr.execute(query)
        result = self.env.cr.dictfetchall()

        df = pd.DataFrame(result)

        # Pivot the DataFrame to convert mail_activity_id to individual columns and sum the count column
        pivot_df = df.pivot_table(index=['team', 'user'], columns='activity', values='count',
                                  aggfunc='sum', fill_value=0).reset_index()

        # Convert pivot table to dictionary
        pivot_dict = pivot_df.to_dict(orient='index')

        # Define the start column (change this as needed)
        col_start = 1  # Column 'B'

        # Write the six columns next to the merged columns
        # sheet.write(row_start + 2, col_start, 'ተ.ቁ', title_format)
        # sheet.write(row_start + 2, col_start+1, 'የሽያጭ ባለሙያው ስም', title_format)

        sheet.merge_range('B3:B4', 'ተ.ቁ', title_format)
        sheet.merge_range('C3:C4', 'የሽያጭ ባለሙያው ስም', title_format)

        sheet.merge_range('D3:G3', 'የአዲስ ደንበኛ ቁጥር', title_format)
        sheet.write("D4", 'ዕቅድ', title_format)
        sheet.merge_range("E4:G4", 'አፈፃፀም', title_format)

        sheet.merge_range('H3:K3', 'የተደወሉ ስልኮች ቁጥር', title_format)
        sheet.write("H4", 'ዕቅድ', title_format)
        sheet.merge_range("I4:K4", 'አፈፃፀም', title_format)

        sheet.merge_range('L3:O3', 'የሳይት እና የቢሮ ጉብኝቶች ቁጥር', title_format)
        sheet.write("L4", 'ዕቅድ', title_format)
        sheet.merge_range("M4:O4", 'አፈፃፀም', title_format)

        sheet.merge_range('P3:S3', 'የሽያጭ ቁጥር', title_format)
        sheet.write("P4", 'ዕቅድ', title_format)
        sheet.merge_range("Q4:S4", 'አፈፃፀም', title_format)

        sheet.merge_range('T3:T4', 'ውጤት', title_format)
        sheet.merge_range('U3:U4', 'አፈፃፀም', title_format)

        new_team = ''
        team_count = 0

        lead_plan = call_plan = visit_plan = sales_plan = 1

        lead_plan_total = lead_result_total = call_plan_total = call_total = visit_plan_total = visit_total = sales_plan_total = sales_total = result_total = 0

        row_start = 4
        no = 1
        team_user_count = 0
        for key in pivot_dict:
            if new_team == '':
                new_team = pivot_dict[key]['team']
                # create new group
                merge_rows = 'B' + str(row_start + 1) + ':U' + str(row_start + 1)
                sheet.merge_range(merge_rows, pivot_dict[key]['team'], title_format2)
                row_start += 1
                team_count += 1
            elif new_team != pivot_dict[key]['team']:
                merge_rows = 'B' + str(row_start + 1) + ':C' + str(row_start + 1)
                sheet.merge_range(merge_rows, new_team + ' Total', title_format2)

                sheet.write(row_start, 3, lead_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 4, lead_result_total, num_format_bold)
                sheet.write(row_start, 5, lead_result_total/lead_plan_total, percent_format)

                # graph formula
                lead_graph_formula = """ REPT("|",""" + str((lead_result_total / lead_plan_total) * 100) + ")"""
                sheet.write_formula(row_start, 6, lead_graph_formula, graph_format_blue)

                sheet.write(row_start, 7, call_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 8, call_total, num_format_bold)
                sheet.write(row_start, 9, call_total / call_plan_total, percent_format)

                # graph formula
                lead_graph_formula = """ REPT("|",""" + str((call_total / call_plan_total) * 100) + ")"""
                sheet.write_formula(row_start, 10, lead_graph_formula, graph_format_burgundy)

                sheet.write(row_start, 11, visit_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 12, visit_total, num_format_bold)
                sheet.write(row_start, 13, visit_total / visit_plan_total, percent_format)

                # graph formula
                lead_graph_formula = """ REPT("|",""" + str((visit_total / visit_plan_total) * 100) + ")"""
                sheet.write_formula(row_start, 14, lead_graph_formula, graph_format_purple)

                sheet.write(row_start, 15, sales_plan_total, num_format_bold)  # plan
                sheet.write(row_start, 16, sales_total, num_format_bold)
                sheet.write(row_start, 17, sales_total / sales_plan_total, percent_format)

                # graph formula
                lead_graph_formula = """ REPT("|",""" + str((sales_total / sales_plan_total) * 100) + ")"""
                sheet.write_formula(row_start, 18, lead_graph_formula, graph_format_red)

                sheet.write(row_start, 19, result_total / team_user_count, percent_format)
                # graph formula
                lead_graph_formula = """ REPT("|",""" + str((result_total / team_user_count) * 100) + ")"""
                sheet.write_formula(row_start, 20, lead_graph_formula, graph_format_green)

                team_user_count = 0

                lead_plan_total = lead_result_total = call_plan_total = call_total = visit_plan_total = visit_total = sales_plan_total = sales_total = result_total = 0

                new_team = pivot_dict[key]['team']
                merge_rows = 'B' + str(row_start + 2) + ':U' + str(row_start + 2)
                sheet.merge_range(merge_rows, pivot_dict[key]['team'], title_format2)
                row_start += 2
                team_count += 1

            # get action plan
            action_plans = self.env["crm.action.plan"].search([('employee.user_id', '=', pivot_dict[key]['user'])])

            for action_plan in action_plans:
                lead_plan = 0
                call_plan = 0
                visit_plan = 0
                sales_plan = 0
                for plan in action_plan.action_details:
                    if plan.plan_type == 'Lead':
                        lead_plan += int(plan.target_number)
                    elif plan.plan_type == 'Site Visit' or plan.plan_type == 'Office Visit':
                        visit_plan += int(plan.target_number)
                    elif plan.plan_type == 'Outgoing Calls':
                        call_plan += int(plan.target_number)
                    elif plan.plan_type == 'Sale':
                        sales_plan += int(plan.target_number)

            sheet.write(row_start, 1, no, num_seq_format)
            sheet.write(row_start, 2, pivot_dict[key]['user'], num_format)

            lead = pivot_dict[key]['Lead']
            sheet.write(row_start, 3, lead_plan, num_format)  # plan
            sheet.write(row_start, 4, lead, num_format)
            sheet.write(row_start, 5, lead / lead_plan, percent_format)
            # graph formula
            lead_graph_formula = """ REPT("|",""" + str((lead / lead_plan) * 100) + ")"""
            sheet.write_formula(row_start, 6, lead_graph_formula, graph_format_blue)

            call = 0

            lead_result = float(pivot_dict[key]['Lead'])

            if 'Outbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Outbound Call'])
            if 'Inbound Call' in pivot_dict[key]:
                call += float(pivot_dict[key]['Inbound Call'])

            sheet.write(row_start, 7, call_plan, num_format)  # plan
            sheet.write(row_start, 8, call, num_format)
            sheet.write(row_start, 9, call / call_plan, percent_format)

            lead_graph_formula = """ REPT("|",""" + str((call / call_plan) * 100) + ")"""
            sheet.write_formula(row_start, 10, lead_graph_formula, graph_format_burgundy)

            visit = 0
            if 'Office Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Office Visit'])
            if 'Site Visit' in pivot_dict[key]:
                visit += float(pivot_dict[key]['Site Visit'])

            sheet.write(row_start, 11, visit_plan, num_format)  # plan
            sheet.write(row_start, 12, visit, num_format)
            sheet.write(row_start, 13, visit / visit_plan, percent_format)

            lead_graph_formula = """ REPT("|",""" + str((visit / visit_plan) * 100) + ")"""
            sheet.write_formula(row_start, 14, lead_graph_formula, graph_format_purple)

            sales = 1
            if 'Closed' in pivot_dict[key]:
                sales += float(pivot_dict[key]['Closed'])

            sheet.write(row_start, 15, sales_plan, num_format)  # plan
            sheet.write(row_start, 16, sales, num_format)
            sheet.write(row_start, 17, sales / sales_plan, percent_format)

            lead_graph_formula = """ REPT("|",""" + str((sales / sales_plan) * 100) + ")"""
            sheet.write_formula(row_start, 18, lead_graph_formula, graph_format_red)

            # result

            result = (lead_result / lead_plan) * 0.1 + (call / call_plan) * 0.15 + (visit / visit_plan) * 0.25 + (
                    sales_plan / sales) * 0.5
            sheet.write(row_start, 19, result, percent_format)

            lead_graph_formula = """ REPT("|",""" + str((result) * 100) + ")"""
            sheet.write_formula(row_start, 20, lead_graph_formula, graph_format_green)

            lead_plan_total += lead_plan
            lead_result_total += lead_result

            call_plan_total += call_plan
            call_total += call

            visit_plan_total += visit_plan
            visit_total += visit

            sales_plan_total += sales_plan
            sales_total += sales

            result_total += result

            row_start += 1
            no += 1
            team_user_count += 1

        merge_rows = 'B' + str(row_start + 1) + ':C' + str(row_start + 1)
        sheet.merge_range(merge_rows, new_team + ' Total', title_format2)

        sheet.write(row_start, 3, lead_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 4, lead_result_total, num_format_bold)
        sheet.write(row_start, 5, lead_result_total / lead_plan_total, percent_format)

        # graph formula
        lead_graph_formula = """ REPT("|",""" + str((lead_result_total / lead_plan_total) * 100) + ")"""
        sheet.write_formula(row_start, 6, lead_graph_formula, graph_format_blue)

        sheet.write(row_start, 7, call_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 8, call_total, num_format_bold)
        sheet.write(row_start, 9, call_total / call_plan_total, percent_format)

        # graph formula
        lead_graph_formula = """ REPT("|",""" + str((call_total / call_plan_total) * 100) + ")"""
        sheet.write_formula(row_start, 10, lead_graph_formula, graph_format_burgundy)

        sheet.write(row_start, 11, visit_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 12, visit_total, num_format_bold)
        sheet.write(row_start, 13, visit_total / visit_plan_total, percent_format)

        # graph formula
        lead_graph_formula = """ REPT("|",""" + str((visit_total / visit_plan_total) * 100) + ")"""
        sheet.write_formula(row_start, 14, lead_graph_formula, graph_format_purple)

        sheet.write(row_start, 15, sales_plan_total, num_format_bold)  # plan
        sheet.write(row_start, 16, sales_total, num_format_bold)
        sheet.write(row_start, 17, sales_total / sales_plan_total, percent_format)

        # graph formula
        lead_graph_formula = """ REPT("|",""" + str((sales_total / sales_plan_total) * 100) + ")"""
        sheet.write_formula(row_start, 18, lead_graph_formula, graph_format_red)

        sheet.write(row_start, 19, result_total / team_user_count, percent_format)
        # graph formula
        lead_graph_formula = """ REPT("|",""" + str((result_total / team_user_count) * 100) + ")"""
        sheet.write_formula(row_start, 20, lead_graph_formula, graph_format_green)

    def get_team_leader(self, team):
        name = ''
        records = self.env["crm.team"].search([('name', '=', team)])
        for record in records:
            name = record.user_id.name

        return name



