odoo.define("champion_crm.dashboard", function (require) {
  "use strict";

  var AbstractAction = require("web.AbstractAction");
  var core = require("web.core");

  var Dashboard = AbstractAction.extend({
    template: "CrmDashboardMain",

    init: function (parent, context) {
      this._super(parent, context);
      this.dashboards_templates = ["CrmLeadsDashboard"];
      this.date_from = moment().subtract(1, "week");
      this.date_to = moment();
      this.name = "Binyam";
    },
  });

  core.action_registry.add("crm_dashboard", Dashboard);

  console.log(Dashboard);
  return Dashboard;
});
