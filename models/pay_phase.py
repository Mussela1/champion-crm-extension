from odoo import models, fields, api

class crm_payment_phases(models.Model):
    _name = 'crm.payphase'

    name = fields.Char("Payment Phase", required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], default='Active')