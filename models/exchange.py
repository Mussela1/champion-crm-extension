from odoo import fields, models, api
from datetime import datetime, timedelta, date
from scrapingant_client import ScrapingAntClient
#from playwright.sync_api import sync_playwright


class exchange_rate(models.Model):
    _name = "crm.exch.rate"
    currency = fields.Char("Currency", required=True)
    exch_date = fields.Date("Exchange Date", required=True)
    buying_rate = fields.Float("Buying Rate (CBE)", digits=(12, 4))
    selling_rate = fields.Float("Selling Rate (CBE)", digits=(12, 4))

    _sql_constraints = [
        ('exch_rate_uniq', 'unique (exch_date,currency)',
         'The exchange rate date per currency must be unique!')
    ]

    def isfloat(self, str):
        try:
            float(str)
            return True
        except ValueError:
            return False

    def fetch_and_create_currency(self):

        #today = datetime.today() + timedelta(hours=3)
        today = date.today()

        if len(self.env['crm.exch.rate'].search(
                [('exch_date', '=', today)])) > 0:
            return

        date_ordinals = {'01': '1st',
                         '02': '2nd',
                         '03': '3rd',
                         '04': '4th',
                         '05': '5th',
                         '06': '6th',
                         '07': '7th',
                         '08': '8th',
                         '09': '9th',
                         '1': '1st',
                         '2': '2nd',
                         '3': '3rd',
                         '4': '4th',
                         '5': '5th',
                         '6': '6th',
                         '7': '7th',
                         '8': '8th',
                         '9': '9th',
                         '10': '10th',
                         '11': '11th',
                         '12': '12th',
                         '13': '13th',
                         '14': '14th',
                         '15': '15th',
                         '16': '16th',
                         '17': '17th',
                         '18': '18th',
                         '19': '19th',
                         '20': '20th',
                         '21': '21st',
                         '22': '22nd',
                         '23': '23rd',
                         '24': '24th',
                         '25': '25th',
                         '26': '26th',
                         '27': '27th',
                         '28': '28th',
                         '29': '29th',
                         '30': '30th',
                         '31': '31st'}

        date_string = today.strftime(
            "%B")+' '+date_ordinals[today.strftime("%d")]+' '+today.strftime("%Y")

        url = "https://www.combanketh.et/en/exchange-rate/"

        client = ScrapingAntClient(token='c17baedc462a4fa89e2f9b18347836a7')

        html = client.general_request(url).content

        # html=""

        # with sync_playwright() as p:
        # browser = p.chromium.launch(channel="chrome",{args: ['--no-sandbox', '--disable-setuid-sandbox']})
        #page = browser.new_page()
        #page_path = "https://www.combanketh.et/en/exchange-rate/"

        # page.goto(page_path)
        #html = page.content()

        # browser.close()

        if date_string not in html:
            if today.strftime("%A") != "Saturday" and today.strftime("%A") != "Sunday":
                return

        to_parse = html[html.find("text-sm text-gray-500"):html.find("text-sm text-gray-500")+2000]
        usdb = to_parse[to_parse.find(
            '</div></div></div></td><td class="py-4 p-4 w-1/4"><div class="text-sm text-gray-900 text-center">') + 128:to_parse.find(
            '</div></div></div></td><td class="py-4 p-4 w-1/4"><div class="text-sm text-gray-900 text-center">') + 135].strip()
        usds = to_parse[to_parse.find(
            '</div></td><td class=" py-4 p-4 w-1/4"><div class="text-sm font-medium text-gray-900 text-center">') + 129: to_parse.find(
            '</div></td><td class=" py-4 p-4 w-1/4"><div class="text-sm font-medium text-gray-900 text-center">') + 136].strip()

        if self.isfloat(usdb) and self.isfloat(usdb) and usdb != 0 and usds != 0:
            values = {
                'buying_rate': usdb.strip(),
                'selling_rate': usds.strip(),
                # 'currency': 'USD',
                'currency': 'USD',
                'exch_date': today
            }

            self.create(values)
