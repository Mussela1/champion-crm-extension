from odoo import models, fields, api
from odoo.exceptions import ValidationError


class property_owners(models.Model):
    _name = 'crm.property.owners'

    name = fields.Char('Developer Name', required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], required=True, default='Active')
    sites = fields.One2many('crm.sites', 'owner_id')


class sites(models.Model):
    _name = 'crm.sites'

    name = fields.Char('Site Name', required=True)
    owner_id = fields.Many2one('crm.property.owners', 'Owner')
    properties = fields.One2many('crm.site.properties', 'site_id')
    building_type = fields.Selection([('Residential', 'Residential'), ('Commercial', 'Commercial'),
                                      ('Residential & Commercial', 'Residential & Commercial')])
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], required=True, default='Active')

    def open_properties(self):
        view = self.env.ref('champion_crm.form_sites_view')

        return {
            'name': 'Properties',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.sites',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }


class site_properties(models.Model):
    _name = 'crm.site.properties'

    property_type = fields.Selection([('Residential', 'Residential'), ('Commercial', 'Commercial')], required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], required=True, default='Active')
    type = fields.Char("Type/Unit", required=True)
    quantity = fields.Integer('Quantity', required=True)
    left_quantity = fields.Integer('Left Quantity', compute="compute_left_quantity")
    sold_quantity = fields.Integer('Left Quantity', compute="compute_left_quantity")
    size = fields.Char('Size/msq', required=True)
    floor = fields.Char("Floor")
    bedrooms = fields.Char('No of Bedrooms')
    site_id = fields.Many2one('crm.sites', 'Sites')
    building_type = fields.Selection(related="site_id.building_type")
    apartment_id = fields.Char("Apartment ID")

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.type + " " + record.size + " " + record.bedrooms + " " + record.floor))
        return result

    def compute_left_quantity(self):
        for record in self:
            count = self.env['crm.sales'].search_count([('property', '=', record.id)])
            record.left_quantity = record.quantity - count
            record.sold_quantity = count
