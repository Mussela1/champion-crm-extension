#from attr import field
from odoo import models, fields, api
from odoo.exceptions import ValidationError, UserError


class champ_pay_commission(models.Model):
    _name = 'crm.sales.commission'
    sales_id = fields.Many2one('crm.sales', required=True, readonly=True)
    sales_person = fields.Many2one(
        related='sales_id.sales_person', store=True, index=True)

    property_owner = fields.Many2one(
        related='sales_id.property_owner', store=True, index=True)
    property_site = fields.Many2one(
        related='sales_id.property_site', store=True, index=True)
    property = fields.Many2one(
        related='sales_id.property', store=True, index=True)

    payment = fields.Many2one('crm.payphase', required=True)
    payment_pct = fields.Float('Payment Percent', required=True)
    champ_pct = fields.Float("Comm'n PCT")
    sales_agent_pct = fields.Float('Sale Agent PCT')
    commission_amount = fields.Float(
        "Comm'n Amount Before Vat", compute="compute_commission", store=True)
    commission_claim = fields.Float(
        "Comm'n To Claim Before Vat", compute="compute_commission", store=True)
    sales_agnet_commission = fields.Float(
        "Sales Agent Comm'n", compute="compute_commission", store=True)
    calculate_excess = fields.Boolean('Calculate Excess?', default=False)
    commission_paid = fields.Float(
        "Comm'n Paid", compute="compute_commision_paid_to_company", store=True)
    commission_paid_agent = fields.Float(
        "Comm'n Paid To Agent", compute="compute_commision_paid_to_agent", store=True)

    # income tax
    income_tax = fields.Float(
        'Income Tax', compute='compute_income_tax', store=True)
    sales_agent_commission_net = fields.Float(
        'Commission Net', compute='compute_income_tax', store=True)

    tax_type = fields.Selection([
        ('15% Vat', '15% Vat'), ('Witholding 2% & 15% Vat', 'Witholding 2% & 15% Vat'), ('Witholding 30% & 15% Vat',
                                                                                         'Witholding 30% & 15% Vat')
    ], string='Tax Type', required=True, default='Witholding 2% & 15% Vat')

    vat_amount = fields.Float(
        'Vat Amount', compute='compute_tax', store=True)
    with_holding_tax = fields.Float(
        'Witholding', compute='compute_tax', store=True)

    commission_claim_total = fields.Float(
        "Comm'n To Claim Total", compute="compute_tax")

    commission_paid_date = fields.Date("Paid Date")
    #is_paid = fields.Boolean('Paid', default=False)
    is_commission_paid = fields.Selection([
        ('Paid', 'Paid'), ('Not Paid', 'Not Paid')
    ], string='Commission Payment Status', default='Not Paid')

    sales_agent_commision_ids = fields.One2many(
        'crm.sales.commission.agent.paid', 'commission_id', string='sales_agent_commision')
    sales_company_commission_ids = fields.One2many(
        'crm.sales.commission.company.paid', 'commission_id', string='sales_company_commission')

    sales_commission_distrubtions = fields.One2many(
        'crm.sales.comission.distrubition', 'commission_dist_id')

    def create(self, vals):
        payment_pct_sum = 0
        champ_pct_sum = 0
        sales_agent_pct_sum = 0
        calc_excess = False
        for rec in vals:
            payment_pct_sum = payment_pct_sum + rec['payment_pct']
            champ_pct_sum = champ_pct_sum + rec['champ_pct']
            sales_agent_pct_sum = sales_agent_pct_sum + rec['sales_agent_pct']
            calc_excess = rec['calculate_excess']
        if payment_pct_sum != 100 or champ_pct_sum != 100 or sales_agent_pct_sum != 100:
            raise UserError(
                "Payment,champion and sales agent commission percent total should be 100.")
        elif calc_excess:
            raise UserError(
                "Excess can not be calculated for the last payment, please correct it..")

        for rec in vals:
            to_create = {
                "sales_id": rec["sales_id"],
                "name": rec["payment"]
            }
            self.env["crm.construction.progress"].create(to_create)
        return super(champ_pay_commission, self).create(vals)

    @api.depends("payment", "payment_pct", "champ_pct", "sales_id.agent_commission_pct",
                 "sales_id.champ_commission_pct", "sales_id.champ_commission_wht", "sales_id.agent_has_license", "sales_id.total_usd_amount", "sales_id.exchange_rate", "sales_id.paid_amount_ETB")
    def compute_commission(self):
        excess_commission = 0.00
        excess_payment = 0.00

        for index, rec in enumerate(self):
            payments = self.env['crm.construction.progress'].search(
                [('sales_id', '=', rec.sales_id.ids), ('name', '=', rec.payment.name)])

            paid = 0.00
            expected_customer_payment = 0.00
            for pay in payments:
                paid = paid + pay["paid_amount_USD"]
                expected_customer_payment = expected_customer_payment + \
                    pay["payment_amount_USD"]

            expected_champ_commision = rec.sales_id.champ_commission_amt * rec.champ_pct * 0.01

            if expected_customer_payment == 0:
                rec.commission_amount = rec.sales_id.champ_commission_amt * rec.champ_pct * 0.01
                rec.commission_claim = 0
                rec.sales_agnet_commission = 0
            elif rec.calculate_excess:
                rec.commission_amount = rec.sales_id.champ_commission_amt * rec.champ_pct * 0.01
                if paid >= expected_customer_payment:
                    excess_payment = (
                        paid - expected_customer_payment) + excess_payment
                    try:
                        excess_commission = ((paid - expected_customer_payment) / (
                            (expected_customer_payment * 100) / rec.payment_pct)) * (
                            rec.sales_id.champ_commission_amt *
                            (self.env["crm.sales.commission"].search(
                                [('sales_id', '=', rec.sales_id.ids)])[
                                index + 1].champ_pct / self.env["crm.sales.commission"].search(
                                [('sales_id', '=', rec.sales_id.ids)])[
                                index + 1].payment_pct)) - excess_commission
                    except Exception as e:
                        excess_commission = 0

                    rec.commission_claim = expected_champ_commision + excess_commission
                    rec.sales_agnet_commission = (rec.commission_claim/rec.commission_amount) * (
                        rec.sales_id.agent_commission_amt * rec.sales_agent_pct * 0.01) if rec.commission_amount else 0
                    # rec.sales_agnet_commission = rec.commission_claim * (
                    #       rec.sales_id.agent_commission_pct / rec.sales_id.champ_commission_pct) if rec.sales_id.champ_commission_pct != 0 else 0
                else:
                    rec.commission_claim = (expected_champ_commision * (
                        (paid + excess_payment) / expected_customer_payment)) - excess_commission
                    rec.sales_agnet_commission = (rec.commission_claim/rec.commission_amount) * (
                        rec.sales_id.agent_commission_amt * rec.sales_agent_pct * 0.01) if rec.commission_amount else 0
                    excess_commission = 0
                    excess_payment = 0
            else:
                rec.commission_amount = rec.sales_id.champ_commission_amt * rec.champ_pct * 0.01
                rec.commission_claim = (
                    expected_champ_commision - excess_commission if (
                        paid + excess_payment) >= expected_customer_payment else (
                        expected_champ_commision * (
                            (
                                paid + excess_payment) / expected_customer_payment)) - excess_commission)
                rec.sales_agnet_commission = (rec.commission_claim/rec.commission_amount) * (
                    rec.sales_id.agent_commission_amt * rec.sales_agent_pct * 0.01) if rec.commission_amount else 0
                excess_commission = 0
                excess_payment = 0
            rec.commission_claim = rec.commission_claim if rec.commission_claim > 0 else 0
            rec.sales_agnet_commission = (rec.commission_claim/rec.commission_amount) * (
                rec.sales_id.agent_commission_amt * rec.sales_agent_pct * 0.01) if rec.commission_amount else 0

    @api.depends('sales_agnet_commission')
    def compute_income_tax(self):
        for record in self:
            if record.sales_agnet_commission <= 600:
                record.income_tax = 0.00
            elif record.sales_agnet_commission <= 1650:
                record.income_tax = (record.sales_agnet_commission*0.1)-60
            elif record.sales_agnet_commission <= 3200:
                record.income_tax = (record.sales_agnet_commission*0.15)-142.5
            elif record.sales_agnet_commission <= 5250:
                record.income_tax = (record.sales_agnet_commission*0.2)-302.5
            elif record.sales_agnet_commission <= 7800:
                record.income_tax = (record.sales_agnet_commission*0.25)-565
            elif record.sales_agnet_commission <= 10900:
                record.income_tax = (record.sales_agnet_commission*0.30)-955
            else:
                record.income_tax = (record.sales_agnet_commission*0.35)-1500

            # net sales commision
            record.sales_agent_commission_net = record.sales_agnet_commission - record.income_tax

    @api.depends('sales_agent_commision_ids')
    def compute_commision_paid_to_agent(self):
        for record in self:
            payments = self.env['crm.sales.commission.agent.paid'].search(
                [('commission_id', 'in', record.ids)])
            sum = 0.00
            for payment in payments:
                if payment.is_paid == "Paid":
                    sum += payment.sales_agent_commission_net

            record.commission_paid_agent = sum

    @api.depends('sales_company_commission_ids')
    def compute_commision_paid_to_company(self):
        for record in self:
            payments = self.env['crm.sales.commission.company.paid'].search(
                [('commission_id', 'in', record.ids)])
            sum = 0.00
            for payment in payments:
                if payment.is_paid == "Paid":
                    sum += payment.company_commission_net

            record.commission_paid = sum

    @api.onchange("commission_claim", "tax_type")
    def compute_tax(self):
        for record in self:

            if record.tax_type == '15% Vat':
                record.vat_amount = record.commission_claim*0.15
                record.with_holding_tax = 0.00
            elif record.tax_type == 'Witholding 2% & 15% Vat':
                record.vat_amount = record.commission_claim*0.15
                record.with_holding_tax = record.commission_claim*0.02
            else:
                record.vat_amount = record.commission_claim*0.15
                record.with_holding_tax = record.commission_claim*0.3

            record.commission_claim_total = record.commission_claim + \
                record.vat_amount-record.with_holding_tax

    def open_sales_agent_commission(self):
        view = self.env.ref(
            'champion_crm.crm_sales_agent_commission_paid_view_form')

        return {
            'name': 'Sales Agent Commission',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.sales.commission',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }

    def open_sales_company_commission(self):
        view = self.env.ref(
            'champion_crm.crm_sales_commission_view_form')

        return {
            'name': 'Champion Commission',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.sales.commission',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }

    def open_commission_distrubtion(self):
        view = self.env.ref(
            'champion_crm.crm_sales_commission_distrubtions_view_form')

        return {
            'name': 'Commission Distrubtion',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.sales.commission',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }


class commission_sales_agent_paid(models.Model):
    _name = 'crm.sales.commission.agent.paid'

    commission_id = fields.Many2one(
        'crm.sales.commission', string='Commision ID')

    # related fields
    sales_person = fields.Many2one(
        related='commission_id.sales_person', index=True, store=True)
    customer = fields.Many2one(
        related='commission_id.sales_id.customer', index=True, store=True)

    property_site = fields.Many2one(
        related='commission_id.sales_id.property_site', index=True, store=True)
    property = fields.Many2one(
        related='commission_id.sales_id.property', index=True, store=True)

    commission_amount = fields.Float(
        'Commision Amount', related='commission_id.sales_agnet_commission', index=True, strore=True)
    tax_type = fields.Selection([
        ('Witholding 2%', 'Witholding 2%'), ("Witholding 2% & 10% TOT", 'Witholding 2% & 10% TOT'), ('Witholding 30%',
                                                                                                     'Witholding 30%'), ('Income Tax', 'Income Tax')
    ], string='Tax Type', required=True, default='Witholding 2%')

    tax_amount = fields.Float(
        'Tax Amount', compute='compute_tax', store=True)
    sales_agent_commission_net = fields.Float(
        'Commission Net', compute='compute_tax', store=True)
    paid_date = fields.Date("Paid Date")
    #is_paid = fields.Boolean('Paid', default=False)
    is_paid = fields.Selection([
        ('Paid', 'Paid'), ('Not Paid', 'Not Paid')
    ], string='Payment Status', default='Not Paid')

    @api.depends('tax_type')
    def compute_tax(self):
        for record in self:

            if record.tax_type == 'Witholding 2%':
                record.tax_amount = record.commission_amount*0.02
                #record.sales_agent_commission_net = record.commission_amount - record.tax_amount
            elif record.tax_type == 'Witholding 30%':
                record.tax_amount = record.commission_amount*0.30
                #record.sales_agent_commission_net = record.commission_amount - record.tax_amount
            elif record.tax_type == 'Witholding 2% & 10% TOT':
                net_amount_to_calculate_wh = record.commission_amount*0.9
                record.tax_amount = net_amount_to_calculate_wh*0.02
            else:
                if record.commission_amount <= 600:
                    record.tax_amount = 0.00
                elif record.commission_amount <= 1650:
                    record.tax_amount = (record.commission_amount*0.1)-60
                elif record.commission_amount <= 3200:
                    record.tax_amount = (record.commission_amount*0.15)-142.5
                elif record.commission_amount <= 5250:
                    record.tax_amount = (record.commission_amount*0.2)-302.5
                elif record.commission_amount <= 7800:
                    record.tax_amount = (record.commission_amount*0.25)-565
                elif record.commission_amount <= 10900:
                    record.tax_amount = (record.commission_amount*0.30)-955
                else:
                    record.tax_amount = (record.commission_amount*0.35)-1500

            # net sales commision
            record.sales_agent_commission_net = record.commission_amount - record.tax_amount


class commission_company_agent_paid(models.Model):

    _name = 'crm.sales.commission.company.paid'

    commission_id = fields.Many2one(
        'crm.sales.commission', string='Commision ID')

    # related fields
    sales_person = fields.Many2one(
        related='commission_id.sales_person', index=True, store=True)
    customer = fields.Many2one(
        related='commission_id.sales_id.customer', index=True, store=True)
    property_owner = fields.Many2one(
        related='commission_id.sales_id.property_owner', index=True, store=True)
    property_site = fields.Many2one(
        related='commission_id.sales_id.property_site', index=True, store=True)
    property = fields.Many2one(
        related='commission_id.sales_id.property', index=True, store=True)
    commission_amount = fields.Float(
        'Commision Amount', related='commission_id.commission_claim', index=True, strore=True)

    tax_type = fields.Selection([
        ('15% Vat', '15% Vat'), ('Witholding 2% & 15% Vat', 'Witholding 2% & 15% Vat'), ('Witholding 30% & 15% Vat',
                                                                                         'Witholding 30% & 15% Vat')
    ], string='Tax Type', required=True, default='15% Vat')

    tax_amount = fields.Float(
        'Tax Amount', compute='compute_tax', store=True)
    with_holding_tax = fields.Float(
        'Witholding', compute='compute_tax', store=True)
    company_commission_net = fields.Float(
        'Commission Net', compute='compute_tax', store=True)
    paid_date = fields.Date("Paid Date")
    #is_paid = fields.Boolean('Paid', default=False)
    is_paid = fields.Selection([
        ('Paid', 'Paid'), ('Not Paid', 'Not Paid')
    ], string='Payment Status', default='Not Paid')

    @api.depends('tax_type')
    def compute_tax(self):
        for record in self:

            if record.tax_type == '15% Vat':
                record.tax_amount = record.commission_amount - \
                    (record.commission_amount/1.15)
                record.with_holding_tax = 0.00
            elif record.tax_type == 'Witholding 2% & 15% Vat':
                record.tax_amount = record.commission_amount - \
                    (record.commission_amount/1.15)
                record.with_holding_tax = (record.commission_amount/1.15)*0.02
            else:
                record.tax_amount = record.commission_amount - \
                    (record.commission_amount/1.15)
                record.with_holding_tax = (record.commission_amount/1.15)*0.3

        record.company_commission_net = record.commission_amount - \
            record.tax_amount-record.with_holding_tax


class commission_request(models.Model):
    _name = 'crm.sales.comm_request'
    sales_id = fields.Many2one('crm.sales', readonly=True)
    commission_pct = fields.Float("Commission Percent")
    has_license = fields.Boolean("Has License", required=True)
    customer = fields.Many2one(
        'res.partner', string='Customer', compute="compute_fill_fields")
    property_site = fields.Many2one(
        'crm.sites', string='Property Site', compute="compute_fill_fields")
    property = fields.Many2one(
        'crm.site.properties', 'Property', compute="compute_fill_fields")
    paid = fields.Boolean('Paid', default=False)
    paid_amount = fields.Float('Paid Amount')
    note = fields.Html('Note')

    def name_get(self):
        result = []
        for record in self:
            result.append(
                (record.id, "Commission Request For " + record.customer.name))
        return result

    @api.depends("sales_id")
    def compute_fill_fields(self):
        for rec in self:
            rec.customer = rec.sales_id.customer
            rec.property_site = rec.sales_id.property_site
            rec.property = rec.sales_id.property


class crm_sales_comission_distrubition(models.Model):
    _name = 'crm.sales.comission.distrubition'

    commission_dist_id = fields.Many2one(
        'crm.sales.commission', string='Commision ID')

    # related fields

    customer = fields.Many2one(
        related='commission_dist_id.sales_id.customer', index=True, store=True)

    property_site = fields.Many2one(
        related='commission_dist_id.sales_id.property_site', index=True, store=True)
    property = fields.Many2one(
        related='commission_dist_id.sales_id.property', index=True, store=True)

    sales_person = fields.Many2one('hr.employee')
    external_sales_person = fields.Char("External Sales Person")
    comission_percent = fields.Float("Comission Percent")
    comission_amount = fields.Float(
        'Comission Amount', compute='compute_comission_amount', store=True)

    tax_type = fields.Selection([
        ('Witholding 2%', 'Witholding 2%'), ("Witholding 2% & 10% TOT", 'Witholding 2% & 10% TOT'), ('Witholding 30%',
                                                                                                     'Witholding 30%'), ('Income Tax', 'Income Tax')
    ], string='Tax Type', default='Witholding 2%')

    tax_amount = fields.Float(
        'Tax Amount', compute='compute_tax', store=True)
    sales_agent_commission_net = fields.Float(
        'Commission Net', compute='compute_tax', store=True)
    paid_date = fields.Date("Paid Date")
    #is_paid = fields.Boolean('Paid', default=False)
    is_paid = fields.Selection([
        ('Paid', 'Paid'), ('Not Paid', 'Not Paid')
    ], string='Payment Status', default='Not Paid')

    @api.depends('comission_percent')
    def compute_comission_amount(self):
        for record in self:
            record.comission_amount = record.commission_dist_id.sales_agnet_commission * \
                record.comission_percent/100

    @api.depends('tax_type')
    def compute_tax(self):
        for record in self:

            if record.tax_type == 'Witholding 2%':
                record.tax_amount = record.comission_amount*0.02
                #record.sales_agent_commission_net = record.commission_amount - record.tax_amount
            elif record.tax_type == 'Witholding 30%':
                record.tax_amount = record.comission_amount*0.30
                #record.sales_agent_commission_net = record.commission_amount - record.tax_amount
            elif record.tax_type == 'Witholding 2% & 10% TOT':
                net_amount_to_calculate_wh = record.comission_amount*0.9
                record.tax_amount = net_amount_to_calculate_wh*0.02
            else:
                if record.comission_amount <= 600:
                    record.tax_amount = 0.00
                elif record.comission_amount <= 1650:
                    record.tax_amount = (record.comission_amount*0.1)-60
                elif record.comission_amount <= 3200:
                    record.tax_amount = (record.comission_amount*0.15)-142.5
                elif record.comission_amount <= 5250:
                    record.tax_amount = (record.comission_amount*0.2)-302.5
                elif record.comission_amount <= 7800:
                    record.tax_amount = (record.comission_amount*0.25)-565
                elif record.comission_amount <= 10900:
                    record.tax_amount = (record.comission_amount*0.30)-955
                else:
                    record.tax_amount = (record.comission_amount*0.35)-1500

            # net sales commision
            record.sales_agent_commission_net = record.comission_amount - record.tax_amount
