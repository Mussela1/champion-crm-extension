from odoo import models, fields, api


class champ_pay_docstatus(models.Model):
    _name = 'crm.sales.docstatus'
    sales_id = fields.Many2one('crm.sales', required=True, readonly=True)
    name = fields.Char("Document Type", required=True)
    note = fields.Char('Note')
    done = fields.Boolean('Done')
