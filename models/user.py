from odoo import models, fields


class User(models.Model):
    _inherit = 'res.users'

    property_developer = fields.Many2one('crm.property.owners', string='Property Developer')
