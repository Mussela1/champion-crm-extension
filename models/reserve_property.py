from odoo import models, fields, api


class reserve_property(models.Model):
    _name = 'crm.reserve.property'

    _inherit = ['mail.thread', 'mail.activity.mixin', 'image.mixin']

    property_site = fields.Many2one('crm.sites', required=True)
    property_owner = fields.Many2one(related='property_site.owner_id', store=True, index=True)
    property = fields.Many2one('crm.site.properties', required=True, domain="[('site_id', '=', property_site)]")
    reserve_until = fields.Date('Date', required=True)
    floor = fields.Char('Floor')
    note = fields.Char('Note')
    status = fields.Selection([('Draft', 'Draft'), ('Rejected', 'Rejected'), ('Approved', 'Approved')], default='Draft')

    def name_get(self):
        result = []
        for record in self:
            result.append(
                (record.id, "Property"))
            return result

    def approve(self):
        self.write({'status': 'Approved'})

        # send message for sales team
        channels = self.env['mail.channel'].search([('name', '=', 'general')])

        odoobot = self.env.ref('base.partner_root')

        for channel in channels:
            channel.message_post(
                subject="Reserved Properties",
                body="Hello, Property at " + str(self.property_site.name) + " - " + str(self.property.type) + " " + str(
                    self.property.size)
                     + " " + str(self.property.bedrooms) + " " + str(self.floor) + " site are reserved by " + str(
                    self.create_uid.name) + " so please don't show to other customers until " + str(self.reserve_until),
                message_type='comment',
                subtype_xmlid='mail.mt_comment',
                author_id=odoobot.id,
            )

        return True

    def reject(self):
        self.write({'status': 'Rejected'})
        return True
