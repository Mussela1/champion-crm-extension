from odoo import models, fields


class lead_source(models.Model):
    _name = 'crm.lead.source'
    name = fields.Char('Name', required=True)
    categories = fields.One2many('crm.lead.source.category', 'source_id', string='Categories')


class lead_source_category(models.Model):
    _name = 'crm.lead.source.category'

    name = fields.Char('Name', required=True)
    source_id = fields.Many2one('crm.lead.source', 'Lead Sources')
    sub_categories_id = fields.One2many('crm.lead.source.sub.category', 'sub_category_id', string='Sub Categories')

    def open_sub_category(self):
        view = self.env.ref('champion_crm.form_lead_source_category_view')

        return {
            'name': 'Sub Category',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.lead.source.category',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }


class lead_source_sub_category(models.Model):
    _name = 'crm.lead.source.sub.category'

    name = fields.Char('Name', required=True)
    sub_category_id = fields.Many2one('crm.lead.source.category', 'Sub Category')
