from odoo import models, fields, api
from datetime import date
from odoo.exceptions import ValidationError


class crm_sales(models.Model):
    _name = 'crm.sales'
    _inherit = ['mail.thread', 'mail.activity.mixin', 'image.mixin']

    _order = 'contract_no asc'

    opportunity_id = fields.Many2one('crm.lead', required=True, readonly=True)
    sales_person = fields.Many2one(
        related='opportunity_id.user_id', store=True, index=True)
    customer = fields.Many2one(
        related='opportunity_id.partner_id', store=True, index=True)
    total_usd_with_vat = fields.Float('USD Total Amount', required=True)
    total_usd_amount = fields.Float(
        'USD Before VAT', compute='computeusdbeforevat', required=True)
    ex_rate_date = fields.Date('Ex Rate Date')
    # total_etb_amount = fields.Float('ETB Total Amount', compute="compute_etb_total", required=True)
    # vat = fields.Float('VAT', compute="compute_etb_total", required=True)
    # total_before_vat = fields.Float('ETB Before VAT', compute="compute_etb_total", required=True)
    # cont_date = fields.Date('Contract Date')
    exchange_rate = fields.Float("Ex Rate", digits=(
        12, 4), compute='fetch_exc_rate', inverse='retain_exc_rate', store=True)
    total_etb_amount = fields.Float(
        'ETB Total Amount', compute="compute_etb_total", required=True)
    vat = fields.Float('VAT', compute="compute_etb_total", required=True)
    total_before_vat = fields.Float(
        'ETB Before VAT', compute="compute_etb_total", required=True)
    contract_no = fields.Char('Contract No')
    cont_date = fields.Date('Contract Date', required=True)
    # exchange_rate = fields.Float("EX Rate", digits=(12, 4))
    property_site = fields.Many2one('crm.sites', required=True)
    property_owner = fields.Many2one(
        related='property_site.owner_id', store=True, index=True)
    property = fields.Many2one(
        'crm.site.properties', required=True, domain="[('site_id', '=', property_site)]")

    size = fields.Char(
        related='property.size', store=True, index=True)
    type = fields.Char(
        related='property.type', store=True, index=True)
    floor = fields.Char(
        related='property.floor', store=True, index=True)
    construction_progress = fields.One2many(
        'crm.construction.progress', 'sales_id', required=True)
    champion_commission = fields.One2many(
        'crm.sales.commission', 'sales_id', required=True)
    document_status = fields.One2many(
        'crm.sales.docstatus', 'sales_id', required=True)
    # agent_commission_request = fields.One2many('crm.sales.comm_request', 'sales_id', required=True)
    paid_amount_ETB = fields.Float(
        "ETB Paid Amount", compute="compute_paid_amount", store=True)
    paid_amount_USD = fields.Float(
        "USD Paid Amount", compute="compute_paid_amount", store=True)
    remaining_payment_ETB = fields.Float(
        "ETB Remaining Amount - Approximate", compute="compute_remaining_amount")
    remaining_payment_USD = fields.Float(
        "USD Remaining Amount", compute="compute_remaining_amount", store=True)
    remaining_pct_total = fields.Float(
        "Remaining % Total", compute="compute_remaining_amount", store=True)
    champ_commission_pct = fields.Float("Champion Commission Percent")
    champ_commission_wht = fields.Boolean("Champion Withholding TAX?")
    champ_commission_amt = fields.Float(
        "Champion Commission Amount", compute="compute_commission_amt")
    agent_commission_pct = fields.Float("S.Agent Comm'n Pct")
    agent_has_license = fields.Boolean("S.Agent Has License?")
    agent_commission_amt = fields.Float(
        "S.Agent Comm'n Amt", compute="compute_commission_amt")
    status_documents = fields.One2many('crm.sales.docstatus', 'sales_id')
    commission_requests = fields.One2many('crm.sales.comm_request', 'sales_id')

    def open_commission(self):
        view = self.env.ref('champion_crm.view_crm_sales_commission')

        return {
            'target': 'new',
            'name': 'Champion Sales Commission',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.sales',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'res_id': self.id
        }

    def open_doc_status(self):
        view = self.env.ref('champion_crm.form_action_doc_status')

        return {
            'target': 'new',
            'name': 'Champion Sales Document Status',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.sales.docstatus',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'res_id': self.id
        }

    def retain_exc_rate(self):
        pass

    @api.depends('ex_rate_date')
    def fetch_exc_rate(self):
        for rec in self:
            # if (rec.exchange_rate == 0):
            daily_rate = self.env['crm.exch.rate'].search(
                [('exch_date', '=', rec.ex_rate_date), ('currency', '=', 'USD')])["selling_rate"]
            rec.exchange_rate = daily_rate if daily_rate != 0 else rec.exchange_rate

    @api.depends('total_usd_with_vat')
    def computeusdbeforevat(self):
        for rec in self:
            rec.total_usd_amount = rec.total_usd_with_vat / 1.15

    @api.depends('champ_commission_pct', 'total_etb_amount', 'champ_commission_wht', 'agent_commission_pct',
                 'agent_has_license')
    def compute_commission_amt(self):
        for sales in self:
            # sales.champ_commission_amt = sales.champ_commission_pct * sales.total_etb_amount * 0.01
            sales.champ_commission_amt = (
                                                 sales.champ_commission_pct / 100) * sales.total_before_vat

            sales.agent_commission_amt = (
                                                 sales.agent_commission_pct / 100) * sales.total_before_vat

            # sales.agent_commission_amt = sales.champ_commission_amt * (sales.agent_commission_pct / sales.champ_commission_pct) * (1 / 1.15) * (0.7) if sales.champ_commission_pct != 0 else 0
            # sales.agent_commission_amt = sales.champ_commission_amt * \
            # (sales.agent_commission_pct / sales.champ_commission_pct) * \
            # (1 / 1.15) * (1) if sales.champ_commission_pct != 0 else 0

    @api.depends('total_usd_amount', 'exchange_rate')
    def compute_etb_total(self):
        for rec in self:
            rec.vat = rec.total_usd_amount * rec.exchange_rate * 0.15
            rec.total_before_vat = rec.total_usd_amount * rec.exchange_rate
            rec.total_etb_amount = rec.total_usd_amount * rec.exchange_rate * 1.15
            rec.champ_commission_amt = rec.champ_commission_pct * rec.total_etb_amount * 0.01

    def _get_current_login_user(self):

        user_obj = self.env['res.users'].search([])

        for user_login in user_obj:

            current_login = self.env.user

            if user_login == current_login:
                return current_login

    @api.model
    def default_get(self, fields_list):
        res = super(crm_sales, self).default_get(fields_list)

        # Get property owners
        user = self._get_current_login_user()

        # Get kpi administrator group id
        group_id = self.env.ref('champion_crm.group_crm_administrator').id

        # Update default valuess
        if user['property_developer'] and group_id not in user['groups_id'].ids:
            res.update(
                {'property_owner': user['property_developer'].id, 'is_user_admin': False})

        return res

    @api.depends("opportunity_id", "construction_progress.paid_amount_ETB")
    def compute_paid_amount(self):
        for record in self:
            payments = self.env['crm.construction.progress'].search(
                [('sales_id', '=', record.ids)])

            sum_payment_etb = 0
            sum_payment_usd = 0
            for payment in payments:
                sum_payment_etb += payment["paid_amount_ETB"]
                sum_payment_usd += payment["paid_amount_USD"]

            record.paid_amount_ETB = sum_payment_etb
            record.paid_amount_USD = sum_payment_usd

    @api.depends("opportunity_id", "construction_progress.paid_amount_ETB", "total_usd_amount", "exchange_rate")
    def compute_remaining_amount(self):
        for record in self:
            record.remaining_payment_USD = (
                                                   record.total_usd_amount * 1.15) - record.paid_amount_USD
            record.remaining_pct_total = (record.remaining_payment_USD / (
                    record.total_usd_amount * 1.15)) * 100 if record.total_usd_amount != 0 else 0

            record.remaining_payment_ETB = self.env['crm.exch.rate'].search(
                [('exch_date', '=', date.today()), ('currency', '=', 'USD')])[
                                               "selling_rate"] * record.remaining_payment_USD

            record.remaining_payment_ETB = (
                    record.total_etb_amount - record.paid_amount_ETB) if (
                    record.remaining_payment_ETB == 0 and record.remaining_payment_USD != 0) else record.remaining_payment_ETB

    def name_get(self):
        result = []
        for record in self:
            result.append((record.id, record.customer.name))
        return result

    @api.constrains('contract_no')
    def unique_contract_no(self):
        for record in self:
            # search crm sales
            count = self.env['crm.sales'].search_count([('contract_no', '=', record.contract_no)])

            if count > 1:
                raise ValidationError("This contract number is already regsitered")


class crm_construction_progress(models.Model):
    _name = 'crm.construction.progress'

    sales_id = fields.Many2one('crm.sales', required=True)
    name = fields.Many2one('crm.payphase', required=True)
    is_done = fields.Boolean("Done", default=False)
    done_date = fields.Date("Payment Date")
    exchange_date = fields.Date("EX rate date")
    payment_amount_USD = fields.Float(
        "Amount USD", compute="fetch_payment_amount")
    # vat = fields.Float("VAT", compute="compute_etb")
    exchange_rate = fields.Float("EX Rate", digits=(12, 4), compute="fetch_exc_rate", inverse='retain_exc_rate',
                                 store=True)
    payment_amount_ETB = fields.Float(
        "Amount ETB", compute="compute_etb", store=True)
    paid_amount_ETB = fields.Float(
        'Paid ETB', compute="compute_paid_amount", store=True)
    paid_amount_USD = fields.Float(
        'Paid USD', compute="compute_paid_amount", store=True)
    remaining_pct_phase = fields.Float(
        'Remaining % For Phase', compute="compute_paid_amount", store=True)
    payments = fields.One2many(
        'crm.construction.progress.payment', 'construction_progress_id', required=True)

    @api.depends('payment_amount_USD', 'exchange_rate')
    def compute_etb(self):
        for record in self:
            # record.vat = record.payment_amount_USD * record.exchange_rate * 0.15
            record.payment_amount_ETB = record.payment_amount_USD * record.exchange_rate

    def retain_exc_rate(self):
        pass

    @api.depends('exchange_date')
    def fetch_exc_rate(self):
        for rec in self:
            # if (rec.exchange_rate == 0):
            daily_rate = self.env['crm.exch.rate'].search(
                [('exch_date', '=', rec.exchange_date), ('currency', '=', 'USD')])["selling_rate"]
            rec.exchange_rate = daily_rate if daily_rate != 0 else rec.exchange_rate

    @api.depends('name')
    def fetch_payment_amount(self):
        pay_percent = 0.00
        for rec in self:
            pay_percentage = rec.env['crm.sales.commission'].search(
                [('sales_id', '=', rec.sales_id.ids), ('payment', '=', rec.name.name)])
            for pay in pay_percentage:
                pay_percent = pay_percent + pay["payment_pct"]

            rec.payment_amount_USD = rec.sales_id.total_usd_with_vat * \
                                     pay_percentage["payment_pct"] * 0.01

    @api.depends('payment_amount_USD', 'exchange_rate', 'payments')
    def compute_paid_amount(self):

        for record in self:
            if record.ids:
                payments = self.env['crm.construction.progress.payment'].search(
                    [('construction_progress_id', '=', record.ids[0])])

                sum_payment_etb = 0
                sum_payment_usd = 0
                for payment in payments:
                    sum_payment_etb += payment["payment_amount"]
                    sum_payment_usd += payment["paid_usd"]

                record.paid_amount_ETB = sum_payment_etb
                record.paid_amount_USD = sum_payment_usd

                # record.remaining_pct_phase = record.payment_amount_ETB - record.paid_amount_ETB
                record.remaining_pct_phase = ((record.payment_amount_USD - record.paid_amount_USD) / (
                    record.payment_amount_USD)) * 100 if record.payment_amount_USD != 0 else 100

    def open_payments(self):
        view = self.env.ref('champion_crm.view_crm_construction_progress_form')

        return {
            'name': 'Payments',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.construction.progress',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }

    def open_payments_report(self):
        view = self.env.ref(
            'champion_crm.view_crm_construction_progress_report_form')

        return {
            'name': 'Payments',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'crm.construction.progress',
            'view_id': view.id,
            'type': 'ir.actions.act_window',
            'target': 'new',
            'res_id': self.id
        }


class crm_construction_progress_payment(models.Model):
    _name = 'crm.construction.progress.payment'

    construction_progress_id = fields.Many2one(
        'crm.construction.progress', required=True, readonly=True)
    bank = fields.Many2one('crm.bank', required=True)
    reference = fields.Char("Reference")
    note = fields.Char("Note")
    payment_date = fields.Date("Payment Date", required=True)
    payment_amount = fields.Float("Payment Amt ETB", required=True)
    pay_exch_rate = fields.Float("EX Rate", digits=(12, 4), compute="fetch_exc_rate", inverse='retain_exc_rate',
                                 store=True)
    paid_usd = fields.Float(
        "Payment Amt USD", compute="calculate_paid_usd", required=True, digits=(12, 4))

    @api.depends('pay_exch_rate', 'payment_amount')
    def calculate_paid_usd(self):
        for rec in self:
            rec.paid_usd = rec.payment_amount / \
                           rec.pay_exch_rate if rec.pay_exch_rate != 0 else 0

    @api.depends('payment_date')
    def fetch_exc_rate(self):
        for rec in self:
            daily_rate = self.env['crm.exch.rate'].search(
                [('exch_date', '=', rec.payment_date), ('currency', '=', 'USD')])["selling_rate"]
            rec.pay_exch_rate = daily_rate if daily_rate != 0 else 0

    def retain_exc_rate(self):
        pass
