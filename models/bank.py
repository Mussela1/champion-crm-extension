from odoo import models, fields, api


class crm_bank_accounts(models.Model):
    _name = 'crm.bank'

    name = fields.Char("Bank Name", required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')], default='Active')
