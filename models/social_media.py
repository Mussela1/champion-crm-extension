from odoo import models, fields, api, _
from datetime import datetime
from odoo.exceptions import ValidationError, UserError


class SocialMedia(models.Model):
    _name = 'crm.social.media.record'

    _description = 'Social Media Statistics for Each Sales Personnel'

    sales_personnel = fields.Many2one('res.users', required=True, default=lambda self: self.env.user)
    date_from = fields.Date('Date From', required=True)
    date_to = fields.Date('Date To', required=True)
    status = fields.Selection([('Active', 'Active'), ('Closed', 'Closed')])
    details = fields.One2many('crm.social.media.record.detail', 'detail_id', string='Details')

    @api.model
    def create(self, vals):
        date_from = datetime.strptime(vals['date_from'], '%Y-%m-%d')
        date_to = datetime.strptime(vals['date_to'], '%Y-%m-%d')

        if date_to < date_from:
            raise UserError(_("Date to can't be less than date from"))

        res = super(SocialMedia, self).create(vals)

        return res


class SocialMediaDetail(models.Model):
    _name = 'crm.social.media.record.detail'

    detail_id = fields.Many2one('crm.social.media.record', string='Details')
    media_type = fields.Selection([('Facebook', 'Facebook'), ('Twitter', 'Twitter'), ('Instagram', 'Instagram')])
    posts = fields.Integer('Number of Posts')
    followers = fields.Integer('Net followers gain or loss')
    likes = fields.Integer('Number of Likes')
    post_reach = fields.Integer('Post Reach')
    no_comments = fields.Integer('Number of Comments')
    no_shares = fields.Integer('Number of Shares')
