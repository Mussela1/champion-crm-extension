from odoo import models, fields, api, _
from odoo.exceptions import ValidationError, UserError
from datetime import datetime


class lead_extension(models.Model):
    _inherit = "crm.lead"
    name = fields.Char("Opportunity", default="-")
    lead_source = fields.Many2one('crm.lead.source', 'Lead Sources')
    lead_source_category = fields.Many2one('crm.lead.source.category', 'Source Category',
                                           domain="[('source_id', '=', lead_source)]")
    lead_source_sub_category = fields.Many2one('crm.lead.source.sub.category', 'Sub Category',
                                               domain="[('sub_category_id', '=', lead_source_category)]")

    client_interest = fields.Selection(
        [('Low', 'Low'), ('Medium', 'Medium'), ('High', 'High')])
    is_started_as_lead = fields.Boolean('Is Started as Lead', default=False)
    property_site = fields.Many2one('crm.sites')
    date_open = fields.Datetime(readonly=False)

    property_owner = fields.Many2one(
        related='property_site.owner_id', store=True, index=True)

    gender = fields.Selection([('Male', 'Male'), ('Female', 'Female')])
    martial_status = fields.Selection([('Single', 'Single'), ('Married', 'Married')])
    buying_motive = fields.Char("Buying Motive")

    @api.model
    def create(self, vals):
        if vals['type'] == "lead":
            vals.update({
                'is_started_as_lead': True
            })
        else:
            vals.update({
                'is_started_as_lead': False
            })

        # if not vals["partner_name"] == "":
        # vals["partner_name"] = vals["name"]

        sales = self.env['crm.lead'].search(
            [('mobile', '=', vals["mobile"]), ('active', '=', 'True')])
        if sales.ids:
            raise UserError(_('The lead is already registered.'))

        # create customer record
        customer = {
            'company_type': 'person',
            'name': vals["name"],
            'mobile': vals["mobile"]}

        # self.env['res.partner'].create(customer)

        res = super(lead_extension, self).create(vals)
        return res

    def write(self, vals):

        for record in self:
            if 'mobile' in vals:
                sales = self.env['crm.lead'].search(
                    [('mobile', '=', vals["mobile"]), ('active', '=', 'True')])
                if sales.ids:
                    raise UserError(
                        _('The mobile number is already registered.'))

            if 'stage_id' in vals:
                if record.stage_id.is_won and vals['stage_id'] != record.stage_id.id:
                    # check if sales are linked with the current opportunities
                    sales = self.env['crm.sales'].search(
                        [('opportunity_id', '=', self.id)])
                    if sales.ids:
                        raise UserError(
                            _('You can not change opportunity linked with sales, contact the sales team'))

        res = super(lead_extension, self).write(vals)

        if 'stage_id' in vals:
            self.add_closed_activity()

        return res

    def open_sales_window(self):
        view = self.env.ref('champion_crm.view_crm_sales_tree')

        return {
            'name': 'Sales',
            'view_mode': 'tree',
            'view_type': 'form',
            'res_model': 'crm.sales',
            'views': [(self.env.ref('champion_crm.view_crm_sales_tree').id, 'tree'),
                      (self.env.ref('champion_crm.view_crm_sales_form').id, 'form')],
            'type': 'ir.actions.act_window',
            'res_id': self.id,
            'context': {
                'default_opportunity_id': self.id,
                'default_customer_id': self.partner_id.id
            }
        }

    @api.onchange('contact_name')
    def compute_lead_description(self):
        for rec in self:
            if rec.contact_name:
                rec.name = "Opportunity for " + str(rec.contact_name)

    def add_closed_activity(self):
        for record in self:
            # check for existing activity

            res_model_id = self.env['ir.model'].search([('model', '=', 'crm.lead')]).id
            activity_type_id = 8

            self.env.cr.execute(
                """ update mail_activity set active='true' where res_id=%s and res_model_id=%s and activity_type_id=%s""",
                (record.id, res_model_id, activity_type_id))

            activities = self.env['mail.activity'].search(
                [('res_id', '=', record.id), ('res_model_id', '=', res_model_id), ('activity_type_id', '=', 8)])

            for activity in activities:
                # unlink the record
                activity.unlink()

            # add new activity
            if record.stage_id.id == 4:
                todos = dict(res_id=self.id,
                             res_model_id=self.env['ir.model'].search(
                                 [('model', '=', 'crm.lead')]).id,
                             user_id=self.create_uid.id, summary='Closed Deals', note='',
                             activity_type_id=8,
                             date_deadline=record.date_deadline, date_done=record.date_deadline, state='done',
                             done=True, active=True)

                activity = self.env['mail.activity'].sudo().create(todos)
                activity.sudo().action_done()

    def close_activity(self):
        close_opprtunities = self.env['crm.lead'].search([('stage_id', '=', 4), ('type', '=', 'opportunity')])

        res_model_id = self.env['ir.model'].search([('model', '=', 'crm.lead')]).id
        activity_type_id = 8

        for record in close_opprtunities:
            # check for existing activity

            self.env.cr.execute(
                """ update mail_activity set active='true' where res_id=%s and res_model_id=%s and activity_type_id=%s""",
                (record.id, res_model_id, activity_type_id))

            activities = self.env['mail.activity'].search(
                [('res_id', '=', record.id), ('res_model_id', '=', res_model_id), ('activity_type_id', '=', 8)])

            for activity in activities:
                # unlink the record
                activity.unlink()

            # add new activity
            if record.stage_id.id == 4:
                todos = dict(res_id=record.id,
                             res_model_id=self.env['ir.model'].search(
                                 [('model', '=', 'crm.lead')]).id,
                             user_id=record.create_uid.id, summary='Closed Deals', note='',
                             activity_type_id=8,
                             date_deadline=record.date_deadline, date_done=record.date_deadline, state='done',
                             done=True, active=True)

                activity = self.env['mail.activity'].sudo().create(todos)
                lead_id = activity.sudo().action_done()

                self.env.cr.execute(
                    """ update mail_message set date=%s where res_id=%s and mail_activity_type_id=%s""",
                    (record.date_deadline, record.id, 8))

