# -*- coding: utf-8 -*-
{
    'name': "CRM Extension Module",

    'summary': """
        This module extension is developed for Champion properties. It extends the CRM functionalities.""",

    'description': """

    """,

    'author': "Afomsoft",

    # Categories can be used to filter modules in modules listing
    # for the full list
    'category': 'Crm',
    'version': '1.0',

    # always loaded
    'data': [
        'security/Security.xml',
        'security/ir.model.access.csv',
        'views/dashboard.xml',
        'views/lead_extension.xml',
        'views/sales.xml',
        'views/lead_source.xml',
        'views/sites.xml',
        'views/social_media.xml',
        'views/action_plan.xml',
        'views/bank.xml',
        'views/pay_phase.xml',
        'views/commission.xml',
        'views/doc_status.xml',
        'views/exchange.xml',
        'views/reserve_property.xml',
        'report/paper_format.xml',
        'report/sales_report.xml',
        'report/performance_report.xml',
        'report/action_plan_report.xml',
        'report/loyal_customer_report.xml',
        'report/commission_report.xml',
        'report/sites.xml',
        'views/menu.xml',
        'views/user.xml',
    ],

    'assets': {
        'web.assets_backend': [
            'champion_crm/static/src/js/dashboard.js',
            'champion_crm/static/src/css/user-rtl.min.css',
            'champion_crm/static/src/css/user.min.css',
        ],

        'web.assets_qweb': [
            'champion_crm/static/src/xml/dashboard.xml',
        ],

    },

    # any module necessary for this one to work correctly

    'depends': ['base', 'mail', 'resource', 'crm', 'hr', 'board'],

    "license": "AGPL-3",
    # only loaded in demonstration mode
    'installable': True,
    'application': True,
    'css': 'static/src/css/css.css'
}
